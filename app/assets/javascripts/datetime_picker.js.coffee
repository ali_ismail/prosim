jQuery ->
  $('.datetimepicker_resources').datetimepicker(                  
          dateFormat : "yy-mm-dd ",
          timeFormat: 'hh:mm:ss',    
          changeMonth : true,
          changeYear : true,
          yearRange : "c-30:c+30",          
       
  )
  $('.datetimepicker_tasklist').datetimepicker(                  
          dateFormat : "yy-mm-dd ",
          timeFormat: 'hh:mm:ss',    
          changeMonth : true,
          changeYear : true,
          yearRange : "c-30:c+30",       
  )
  $('.datetimepicker_sim_events').datetimepicker(                  
          dateFormat : "yy-mm-dd ",
          timeFormat: 'hh:mm:ss',    
          changeMonth : true,
          changeYear : true,
          yearRange : "c-30:c+10",          
          minDate : MinDate,
          maxDate : MaxDate,             
  )
