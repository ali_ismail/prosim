# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/
jQuery ->
  $('.datetimepicker_resources').datetimepicker(        
          dateFormat : "yy-mm-dd ",
          timeFormat: 'hh:mm:ss',    
          changeMonth : true,
          changeYear : true,
          yearRange : "c-20:c+20",             
  )