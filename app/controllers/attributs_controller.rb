class AttributsController < ApplicationController
  # GET /attributs
  # GET /attributs.json
  before_filter :authenticate_user!
  def index
    @attributs = Attribut.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @attributs }
      format.xml { render xml: @attributs }
    end
  end

  # GET /attributs/1
  # GET /attributs/1.json
  def show
    @attribut = Attribut.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @attribut }
    end
  end

  # GET /attributs/new
  # GET /attributs/new.json
  def new
    @attribut = Attribut.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @attribut }
    end
  end

  # GET /attributs/1/edit
  def edit
    @attribut = Attribut.find(params[:id])
  end

  # POST /attributs
  # POST /attributs.json
  def create
    @attribut = Attribut.new(params[:attribut])

    respond_to do |format|
      if @attribut.save
        format.html { redirect_to @attribut, notice: 'Attribut was successfully created.' }
        format.json { render json: @attribut, status: :created, location: @attribut }
      else
        format.html { render action: "new" }
        format.json { render json: @attribut.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /attributs/1
  # PUT /attributs/1.json
  def update
    @attribut = Attribut.find(params[:id])

    respond_to do |format|
      if @attribut.update_attributes(params[:attribut])
        format.html { redirect_to @attribut, notice: 'Attribut was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @attribut.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /attributs/1
  # DELETE /attributs/1.json
  def destroy
    @attribut = Attribut.find(params[:id])
    @attribut.destroy

    respond_to do |format|
      format.html { redirect_to attributs_url }
      format.json { head :no_content }
    end
  end
end
