class AwsController < ApplicationController
  # GET /aws
  # GET /aws.json
  before_filter :authenticate_user!
  def index
    @aws = Aw.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @aws }
    end
  end

  # GET /aws/1
  # GET /aws/1.json
  def show
    @aw = Aw.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @aw }
    end
  end

  # GET /aws/new
  # GET /aws/new.json
  def new
    @aw = Aw.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @aw }
    end
  end

  # GET /aws/1/edit
  def edit
    @aw = Aw.find(params[:id])
  end

  # POST /aws
  # POST /aws.json
  def create
    @aw = Aw.new(params[:aw])

    respond_to do |format|
      if @aw.save
        format.html { redirect_to @aw, notice: 'Aw was successfully created.' }
        format.json { render json: @aw, status: :created, location: @aw }
      else
        format.html { render action: "new" }
        format.json { render json: @aw.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /aws/1
  # PUT /aws/1.json
  def update
    @aw = Aw.find(params[:id])

    respond_to do |format|
      if @aw.update_attributes(params[:aw])
        format.html { redirect_to @aw, notice: 'Aw was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @aw.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /aws/1
  # DELETE /aws/1.json
  def destroy
    @aw = Aw.find(params[:id])
    @aw.destroy

    respond_to do |format|
      format.html { redirect_to aws_url }
      format.json { head :no_content }
    end
  end
end
