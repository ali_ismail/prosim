class BimFiltersController < ApplicationController
  # GET /bim_filters
  # GET /bim_filters.json
  before_filter :authenticate_user!
  def index
    @bim_filters = BimFilter.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @bim_filters }
      format.xml { render xml: @bim_filters }
    end
  end

  # GET /bim_filters/1
  # GET /bim_filters/1.json
  def show
    @bim_filter = BimFilter.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @bim_filter }
    end
  end

  # GET /bim_filters/new
  # GET /bim_filters/new.json
  def new
    @bim_filter = BimFilter.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @bim_filter }
    end
  end

  # GET /bim_filters/1/edit
  def edit
    @bim_filter = BimFilter.find(params[:id])
  end

  # POST /bim_filters
  # POST /bim_filters.json
  def create
    @bim_filter = BimFilter.new(params[:bim_filter])

    respond_to do |format|
      if @bim_filter.save
        format.html { redirect_to @bim_filter, notice: 'Bim filter was successfully created.' }
        format.json { render json: @bim_filter, status: :created, location: @bim_filter }
      else
        format.html { render action: "new" }
        format.json { render json: @bim_filter.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /bim_filters/1
  # PUT /bim_filters/1.json
  def update
    @bim_filter = BimFilter.find(params[:id])

    respond_to do |format|
      if @bim_filter.update_attributes(params[:bim_filter])
        format.html { redirect_to @bim_filter, notice: 'Bim filter was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @bim_filter.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /bim_filters/1
  # DELETE /bim_filters/1.json
  def destroy
    @bim_filter = BimFilter.find(params[:id])
    @bim_filter.destroy

    respond_to do |format|
      format.html { redirect_to bim_filters_url }
      format.json { head :no_content }
    end
  end
end
