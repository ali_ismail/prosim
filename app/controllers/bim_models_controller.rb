class BimModelsController < ApplicationController
  # GET /bim_models
  # GET /bim_models.json
  before_filter :authenticate_user!
  def index
    @bim_models = BimModel.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @bim_models }
    end
  end

  # GET /bim_models/1
  # GET /bim_models/1.json
  def show
    @bim_model = BimModel.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @bim_model }
    end
  end

  # GET /bim_models/new
  # GET /bim_models/new.json
  def new
    @bim_model = BimModel.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @bim_model }
    end
  end

  # GET /bim_models/1/edit
  def edit
    @bim_model = BimModel.find(params[:id])
  end

  # POST /bim_models
  # POST /bim_models.json
  def create
    @bim_model = BimModel.new(params[:bim_model])

    respond_to do |format|
      if @bim_model.save
        format.html { redirect_to @bim_model, notice: 'Bim model was successfully created.' }
        format.json { render json: @bim_model, status: :created, location: @bim_model }
      else
        format.html { render action: "new" }
        format.json { render json: @bim_model.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /bim_models/1
  # PUT /bim_models/1.json
  def update
    @bim_model = BimModel.find(params[:id])

    respond_to do |format|
      if @bim_model.update_attributes(params[:bim_model])
        format.html { redirect_to @bim_model, notice: 'Bim model was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @bim_model.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /bim_models/1
  # DELETE /bim_models/1.json
  def destroy
    @bim_model = BimModel.find(params[:id])
    @bim_model.destroy

    respond_to do |format|
      format.html { redirect_to bim_models_url }
      format.json { head :no_content }
    end
  end
end
