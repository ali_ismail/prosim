class BuildingElementsController < ApplicationController
  before_filter :authenticate_user!
  # GET /building_elements
  # GET /building_elements.json
  def index
    @building_elements = BuildingElement.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @building_elements }
    end
  end

  # GET /building_elements/1
  # GET /building_elements/1.json
  def show
    @building_element = BuildingElement.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @building_element }
    end
  end

  # GET /building_elements/new
  # GET /building_elements/new.json
  def new
    @building_element = BuildingElement.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @building_element }
    end
  end

  # GET /building_elements/1/edit
  def edit
    @building_element = BuildingElement.find(params[:id])
  end

  # POST /building_elements
  # POST /building_elements.json
  def create
    @building_element = BuildingElement.new(params[:building_element])

    respond_to do |format|
      if @building_element.save
        format.html { redirect_to @building_element, notice: 'Building element was successfully created.' }
        format.json { render json: @building_element, status: :created, location: @building_element }
      else
        format.html { render action: "new" }
        format.json { render json: @building_element.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /building_elements/1
  # PUT /building_elements/1.json
  def update
    @building_element = BuildingElement.find(params[:id])

    respond_to do |format|
      if @building_element.update_attributes(params[:building_element])
        format.html { redirect_to @building_element, notice: 'Building element was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @building_element.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /building_elements/1
  # DELETE /building_elements/1.json
  def destroy
    @building_element = BuildingElement.find(params[:id])
    @building_element.destroy

    respond_to do |format|
      format.html { redirect_to building_elements_url }
      format.json { head :no_content }
    end
  end
end
