class ConstructionSitesController < ApplicationController
  # GET /construction_sites
  # GET /construction_sites.json
  before_filter :authenticate_user!
  def index    
    @sim_model = SimModel.find(params[:sim_model_id])
    @construction_sites = ConstructionSite.find_all_by_sim_model_id(@sim_model.id)

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @construction_sites }
    end
  end

  # GET /construction_sites/1
  # GET /construction_sites/1.json
  def show
    @sim_model = SimModel.find(params[:sim_model_id])
    @construction_site = ConstructionSite.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @construction_site }
    end
  end

  # GET /construction_sites/new
  # GET /construction_sites/new.json
  def new
   @sim_model = SimModel.find(params[:sim_model_id])
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @construction_site }
    end
  end

  # GET /construction_sites/1/edit
  def edit
    @sim_model = SimModel.find(params[:sim_model_id])
    @construction_site = ConstructionSite.find(params[:id])
  end

  # POST /construction_sites
  # POST /construction_sites.json
  def create
    @sim_model = SimModel.find(params[:sim_model_id])
    @construction_site = @sim_model.construction_site.new(params[:construction_site])
    respond_to do |format|
      if @construction_site.save
        format.html { redirect_to sim_model_construction_sites_path, notice: 'construction site was successfully created.' }
        format.json { render json: @construction_site, status: :created, location: @construction_site }
      else
        format.html { render action: "new" }
        format.json { render json: @construction_site.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /construction_sites/1
  # PUT /construction_sites/1.json
  def update
    @construction_site = ConstructionSite.find(params[:id])

    respond_to do |format|
      if @construction_site.update_attributes(params[:construction_site])
        format.html { redirect_to sim_model_construction_sites_path, notice: 'construction site was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @construction_site.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /construction_sites/1
  # DELETE /construction_sites/1.json
  def destroy
    @construction_site = ConstructionSite.find(params[:id])
    @construction_site.destroy

    respond_to do |format|
      format.html { redirect_to sim_model_construction_sites_path }
      format.json { head :no_content }
    end
  end
end
