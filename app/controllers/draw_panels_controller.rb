class DrawPanelsController < ApplicationController
  before_filter :authenticate_user!
  # GET /draw_panels
  # GET /draw_panels.json
  def index
    @sim_model = SimModel.find(params[:sim_model_id])
    @draw_panels =DrawPanel.find_all_by_sim_model_id(@sim_model.id)
    if @draw_panels.size == 1
      redirect_to "/sim_models/" + @sim_model.id.to_s + "/draw_panels/" + @draw_panels.first.id.to_s
      return
    end
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @draw_panels }
    end
  end

  # GET /draw_panels/1
  # GET /draw_panels/1.json
  def show
    @sim_model = SimModel.find(params[:sim_model_id])    
    @draw_panel = DrawPanel.find(params[:id])    
    #@draw_panel = DrawPanel.find_by_sim_model_id(params[:id])
   # if @draw_panel == nil
    #  @draw_panel =DrawPanel.new(:name => "DrawTafel" , :sim_model_id =>params[:id])
    # @draw_panel.save!
    #end        
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @draw_panel }
    end
  end
  
  
  # GET /draw_panels/1
  # GET /draw_panels/1.json
  
  def show_all
    @draw_panel = DrawPanel.all   
    @img=[]
    i=2
    while i < 400
    @img << "<img width='200' src='http://bci52.cib.bau.tu-dresden.de:3000/simweb1/cache/Flugsteig_Angebot/DrawTafel" + i.to_s + ".png' >"
    i = i +1
    end
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @draw_panel }
    end
  end

  # GET /draw_panels/new
  # GET /draw_panels/new.json
  def new
    @sim_model = SimModel.find(params[:sim_model_id])
    #@draw_panel = DrawPanel.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @draw_panel }
    end
  end

  # GET /draw_panels/1/edit
  def edit
    @sim_model = SimModel.find(params[:sim_model_id])
    @draw_panel = DrawPanel.find(params[:id])
  end

  # POST /draw_panels
  # POST /draw_panels.json
  def create
    @sim_model = SimModel.find(params[:sim_model_id])
    @draw_panel = @sim_model.draw_panels.new(params[:draw_panel])

    respond_to do |format|
      if @draw_panel.save
        format.html { redirect_to sim_model_draw_panels_path, notice: 'Draw panel was successfully created.' }
        format.json { render json: @draw_panel, status: :created, location: @draw_panel }
      else
        format.html { render action: "new" }
        format.json { render json: @draw_panel.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /draw_panels/1
  # PUT /draw_panels/1.json
  def update
    @draw_panel = DrawPanel.find(params[:id])

    respond_to do |format|
      if @draw_panel.update_attributes(params[:draw_panel])
        format.html { redirect_to sim_model_draw_panels_path, notice: 'Draw panel was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @draw_panel.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /draw_panels/1
  # DELETE /draw_panels/1.json
  def destroy
    @draw_panel = DrawPanel.find(params[:id])
    @draw_panel.destroy

    respond_to do |format|
      format.html { redirect_to sim_model_draw_panels_path }
      format.json { head :no_content }
    end
  end
end
