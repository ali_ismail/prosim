class EventsReportsController < ApplicationController
  # GET /rpms
  # GET /rpms.json
  before_filter :authenticate_user!
  def index
    @events_report = EventsReport.group("duration_hours").all
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @rpms }
    end
  end 
end
