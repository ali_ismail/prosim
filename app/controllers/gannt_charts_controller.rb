class GanntChartsController < ApplicationController
  before_filter :authenticate_user!
 
 
  # GET /gannt_charts
  # GET /gannt_charts.json
  def index
    @sim_model = SimModel.find(params[:sim_model_id])
    @gannt_charts = GanntChart.find_all_by_sim_model_id(@sim_model.id)
    if @gannt_charts.size == 1
      redirect_to "/sim_models/" + @sim_model.id.to_s + "/gannt_charts/" + @gannt_charts.first.id.to_s
      return
    end
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @gannt_charts }
    end
  end

  # GET /gannt_charts/1
  # GET /gannt_charts/1.json
  def show
    @sim_model = SimModel.find(params[:sim_model_id])
    @gannt_chart = GanntChart.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @gannt_chart }
    end
  end

  # GET /gannt_charts/new
  # GET /gannt_charts/new.json
  def new
    @sim_model = SimModel.find(params[:sim_model_id])
    @gannt_chart = GanntChart.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @gannt_chart }
    end
  end

  # GET /gannt_charts/1/edit
  def edit
    @sim_model = SimModel.find(params[:sim_model_id])
    @gannt_chart = GanntChart.find(params[:id])
  end

  # POST /gannt_charts
  # POST /gannt_charts.json
  def create
    @sim_model = SimModel.find(params[:sim_model_id])
    @gannt_chart = @sim_model.gannt_charts.new(params[:gannt_chart])

    respond_to do |format|
      if @gannt_chart.save
        format.html { redirect_to sim_model_gannt_charts_path, notice: 'Gannt chart was successfully created.' }
        format.json { render json: sim_model_gannt_charts_path, status: :created, location: @gannt_chart }
      else
        format.html { render action: "new" }
        format.json { render json: @gannt_chart.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /gannt_charts/1
  # PUT /gannt_charts/1.json
  def update
    @gannt_chart = GanntChart.find(params[:id])

    respond_to do |format|
      if @gannt_chart.update_attributes(params[:gannt_chart])
        format.html { redirect_to sim_model_gannt_charts_path, notice: 'Gannt chart was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @gannt_chart.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /gannt_charts/1
  # DELETE /gannt_charts/1.json
  def destroy
    @gannt_chart = GanntChart.find(params[:id])
    @gannt_chart.destroy

    respond_to do |format|
      format.html { redirect_to sim_model_gannt_charts_path }
      format.json { head :no_content }
    end
  end
end
