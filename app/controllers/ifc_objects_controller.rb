class IfcObjectsController < ApplicationController
  # GET /ifc_objects
  # GET /ifc_objects.json
  before_filter :authenticate_user!
  def index    
    @search=IfcObject.search(params[:q])
    @ifc_objects =@search.result.paginate(:page => params[:page], :per_page => 20) 
    @search.build_condition
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @ifc_objects }
    end
  end

  # GET /ifc_objects/1
  # GET /ifc_objects/1.json
  def show
    @ifc_object = IfcObject.find(params[:id])
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @ifc_object }
    end
  end

  # GET /ifc_objects/new
  # GET /ifc_objects/new.json
  def new
    @ifc_object = IfcObject.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @ifc_object }
    end
  end

  # GET /ifc_objects/1/edit
  def edit
    @ifc_object = IfcObject.find(params[:id])
  end
  
  
  def svg_viewer          
      svg_width=1024
      svg_height=768
      scale=2.75
      x0=750
      y0=200          
      $img = Rasem::SVGImage.new(svg_width,svg_height) 
      $img.rectangle 0, 0, svg_width, svg_height,:opacity=>0.68999999000000001, :fill=>'#ececec',:fill_opacity=>0.0, :stroke=>'#31001b', :stroke_width => 0.40  
      
      @ifc_object=Bbox.find(:all, :conditions => ["code like ?" ,"L=13.54m%"])
      @ifc_object.each { |o|      
      x1=x0+scale* o.Xmin;
      y1=y0+ scale* o.Ymax;
      $img.rectangle x1,y1, scale*o.DX, scale*(o.DY) , :fill => '#ff0000'
      }
      
      
      @ifc_object=Bbox.find(:all, :conditions => ["code like ?" ,"L=38.40m%"])
      @ifc_object.each { |o|      
      #$img.rectangle 0, 0, svg_width, svg_height,:opacity=>0.68999999000000001, :fill=>'#ececec',:fill_opacity=>0.0, :stroke=>'#31001b', :stroke_width => 0.40
      x1=x0+scale* o.Xmin;
      y1=y0+ scale* o.Ymax; 
      $img.rectangle x1,y1, scale*o.DX, scale*(o.DY) , :fill => '#00ffff'
      }
      @ifc_object=Bbox.find(:all, :conditions => ["code like ?" ,"rc %"])
      @ifc_object.each { |o|      
      #$img.rectangle 0, 0, svg_width, svg_height,:opacity=>0.68999999000000001, :fill=>'#ececec',:fill_opacity=>0.0, :stroke=>'#31001b', :stroke_width => 0.40
      x1=x0+scale* o.Xmin;
      y1=y0+ scale* o.Ymax; 
      $img.rectangle x1,y1, scale*o.DX, scale*(o.DY) , :opacity=>0.40 , :fill => '#D3D3D3'
      }
      @ifc_object=Bbox.find(:all, :conditions => ["shape = 'circle' and dx > 120.0"])
      @ifc_object.each { |o|      
      #$img.rectangle 0, 0, svg_width, svg_height,:opacity=>0.68999999000000001, :fill=>'#ececec',:fill_opacity=>0.0, :stroke=>'#31001b', :stroke_width => 0.40
      x1=x0+scale* o.Xmin;
      y1=y0+ scale* o.Ymax; 
      $img.circle x1+2*o.DX,y1-o.DY, scale*o.DX*0.5 ,:opacity=>0.25,:fill=>'#fbfccd',:stroke=> '#ff0000', :stroke_width => 0.75
      }
     # @ifc_object=Bbox.find(:all, :conditions => {:globalid => @sim_event.object})
     # @ifc_object.each { |o|      
     # x1=x0+scale* o.Xmin;
     # y1=y0+ scale* o.Ymax; 
     # $img.line 0,y1+scale*o.DY*0.5,svg_width,y1+scale*o.DY*0.5,:stroke=>'#31001b', :stroke_width => 0.40
     # $img.line x1+scale*o.DX*0.5,0,x1+scale*o.DX*0.5,svg_height,:stroke=>'#31001b', :stroke_width => 0.40
     # $img.rectangle x1,y1, scale*o.DX, scale*(o.DY) ,:opacity=>0.5, :fill => '#ffaaaa',:stroke=> '#ff0000', :stroke_width => 1.0
     # }
     #sql = ActiveRecord::Base.connection      
     #@object_atts= sql.select_all "select * from ifc_objects where globalId='" + @sim_event.object + "'"    
    @svg=$img.output    
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @sim_event }
    end

  end
  
  
  def count_by_code()  
   sql = ActiveRecord::Base.connection
    @ifc_objects= sql.select_all "select  code as code,count(*) as count  from ifc_Objects group by code order by count(*) DESC"     
  end
  
  
  def count_by_code_by_floor()  
   sql = ActiveRecord::Base.connection
    @ifc_objects= sql.select_all "select  code as code,_4D_Level as Floor, count(*) as count  from ifc_Objects group by code,_4D_Level order by Floor"     
  end
  
  # POST /ifc_objects
  # POST /ifc_objects.json
  def create
    @ifc_object = IfcObject.new(params[:ifc_object])

    respond_to do |format|
      if @ifc_object.save
        format.html { redirect_to @ifc_object, notice: 'Ifc object was successfully created.' }
        format.json { render json: @ifc_object, status: :created, location: @ifc_object }
      else
        format.html { render action: "new" }
        format.json { render json: @ifc_object.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /ifc_objects/1
  # PUT /ifc_objects/1.json
  def update
    @ifc_object = IfcObject.find(params[:id])

    respond_to do |format|
      if @ifc_object.update_attributes(params[:ifc_object])
        format.html { redirect_to @ifc_object, notice: 'Ifc object was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @ifc_object.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /ifc_objects/1
  # DELETE /ifc_objects/1.json
  def destroy
    @ifc_object = IfcObject.find(params[:id])
    @ifc_object.destroy

    respond_to do |format|
      format.html { redirect_to ifc_objects_url }
      format.json { head :no_content }
    end
  end
end
