class LinkModelsController < ApplicationController
  before_filter :authenticate_user!
  # GET /link_models
  # GET /link_models.json
  def index
    @mmc = MultiModelContainer.find(params[:multi_model_container_id])
    @link_models = LinkModel.find_all_by_multi_model_container_id(@mmc.id)

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @link_models }
    end
  end

  # GET /link_models/1
  # GET /link_models/1.json
  def show
    @mmc = MultiModelContainer.find(params[:multi_model_container_id])
    @link_model = LinkModel.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @link_model }
    end
  end

  # GET /link_models/new
  # GET /link_models/new.json
  def new
    @mmc = MultiModelContainer.find(params[:multi_model_container_id])    
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @link_model }
    end
  end

  # GET /link_models/1/edit
  def edit
    @mmc = MultiModelContainer.find(params[:multi_model_container_id])
    @link_model = LinkModel.find(params[:id])
  end

  # POST /link_models
  # POST /link_models.json
  def create
    @mmc = MultiModelContainer.find(params[:multi_model_container_id])
    @link_model = @mmc.link_models.new(params[:link_model])

    respond_to do |format|
      if @link_model.save
        format.html { redirect_to multi_model_container_link_models_path, notice: 'Link model was successfully created.' }
        format.json { render json: multi_model_container_link_models_path, status: :created, location: @link_model }
      else
        format.html { render action: "new" }
        format.json { render json: @link_model.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /link_models/1
  # PUT /link_models/1.json
  def update
    @link_model = LinkModel.find(params[:id])

    respond_to do |format|
      if @link_model.update_attributes(params[:link_model])
        format.html { redirect_to multi_model_container_link_models_path, notice: 'Link model was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @link_model.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /link_models/1
  # DELETE /link_models/1.json
  def destroy
    @link_model = LinkModel.find(params[:id])
    @link_model.destroy

    respond_to do |format|
      format.html { redirect_to multi_model_container_link_models_path }
      format.json { head :no_content }
    end
  end
end
