class LvItemsController < ApplicationController
  before_filter :authenticate_user!
  # GET /lv_items
  # GET /lv_items.json
  def index
    @lv_items = LvItem.find(:all, :order =>:rNOpaer)
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @lv_items }
    end
  end

  # GET /lv_items/1
  # GET /lv_items/1.json
  def show
    @lv_item = LvItem.find(params[:id])
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @lv_item }
    end
  end

  # GET /lv_items/new
  # GET /lv_items/new.json
  def new
    @lv_item = LvItem.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @lv_item }
    end
  end

  # GET /lv_items/1/edit
  def edit
    @lv_item = LvItem.find(params[:id])
  end

  # POST /lv_items
  # POST /lv_items.json
  def create
    @lv_item = LvItem.new(params[:lv_item])

    respond_to do |format|
      if @lv_item.save
        format.html { redirect_to @lv_item, notice: 'Lv item was successfully created.' }
        format.json { render json: @lv_item, status: :created, location: @lv_item }
      else
        format.html { render action: "new" }
        format.json { render json: @lv_item.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /lv_items/1
  # PUT /lv_items/1.json
  def update
    @lv_item = LvItem.find(params[:id])

    respond_to do |format|
      if @lv_item.update_attributes(params[:lv_item])
        format.html { redirect_to @lv_item, notice: 'Lv item was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @lv_item.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /lv_items/1
  # DELETE /lv_items/1.json
  def destroy
    @lv_item = LvItem.find(params[:id])
    @lv_item.destroy

    respond_to do |format|
      format.html { redirect_to lv_items_url }
      format.json { head :no_content }
    end
  end
end
