class MmContentsController < ApplicationController
  before_filter :authenticate_user!
  # GET /mm_contents
  # GET /mm_contents.json
  def index
    @primary_model =PrimaryModel.find(params[:primary_model_id])
    @mm_contents = MmContent.find_all_by_primary_model_id(@primary_model.id)

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @mm_contents }
    end
  end

  # GET /mm_contents/1
  # GET /mm_contents/1.json
  def show
    @primary_model =PrimaryModel.find(params[:primary_model_id])
    @mm_content = MmContent.find(params[:id])
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @mm_content }
    end
  end

  # GET /mm_contents/new
  # GET /mm_contents/new.json
  def new
    @primary_model =PrimaryModel.find(params[:primary_model_id])
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @mm_content }
    end
  end

  # GET /mm_contents/1/edit
  def edit
    @primary_model =PrimaryModel.find(params[:primary_model_id])
    @mm_content = MmContent.find(params[:id])
  end

  # POST /mm_contents
  # POST /mm_contents.json
  def create
    @primary_model =PrimaryModel.find(params[:primary_model_id])
    @mm_content = @primary_model.mm_contents.new(params[:mm_content])

    respond_to do |format|
      if @mm_content.save
        format.html { redirect_to multi_model_container_primary_model_mm_contents_path, notice: 'Mm content was successfully created.' }
        format.json { render json: multi_model_container_primary_model_mm_contents_path, status: :created, location: @mm_content }
      else
        format.html { render action: "new" }
        format.json { render json: @mm_content.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /mm_contents/1
  # PUT /mm_contents/1.json
  def update
    @primary_model =PrimaryModel.find(params[:primary_model_id])
    @mm_content = MmContent.find(params[:id])

    respond_to do |format|
      if @mm_content.update_attributes(params[:mm_content])
        format.html { redirect_to multi_model_container_primary_model_mm_contents_path, notice: 'Mm content was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @mm_content.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /mm_contents/1
  # DELETE /mm_contents/1.json
  def destroy
    @mm_content = MmContent.find(params[:id])
    @mm_content.destroy

    respond_to do |format|
      format.html { redirect_to multi_model_container_primary_model_mm_contents_path }
      format.json { head :no_content }
    end
  end
end
