class MmFilesController < ApplicationController
  before_filter :authenticate_user!
  # GET /mm_files
  # GET /mm_files.json
  def index
    @mm_content =MmContent.find(params[:mm_content_id])
    @mm_files = MmFile.find_all_by_mm_content_id(@mm_content.id)

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @mm_files }
    end
  end

  # GET /mm_files/1
  # GET /mm_files/1.json
  def show
    @mm_content =MmContent.find(params[:mm_content_id])
    @mm_file = MmFile.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @mm_file }
    end
  end

  # GET /mm_files/new
  # GET /mm_files/new.json
  def new
    @mm_content =MmContent.find(params[:mm_content_id])
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @mm_file }
    end
  end

  # GET /mm_files/1/edit
  def edit
    @mm_content =MmContent.find(params[:mm_content_id])
    @mm_file = MmFile.find(params[:id])
  end

  # POST /mm_files
  # POST /mm_files.json
  def create
    @mm_content =MmContent.find(params[:mm_content_id])
    @mm_file = @mm_content.mm_files.new(params[:mm_file])

    respond_to do |format|
      if @mm_file.save
        format.html { redirect_to multi_model_container_primary_model_mm_content_mm_files_path, notice: 'Mm file was successfully created.' }
        format.json { render json: multi_model_container_primary_model_mm_content_mm_files_path, status: :created, location: @mm_file }
      else
        format.html { render action: "new" }
        format.json { render json: @mm_file.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /mm_files/1
  # PUT /mm_files/1.json
  def update
    @mm_content =MmContent.find(params[:mm_content_id])
    @mm_file = MmFile.find(params[:id])

    respond_to do |format|
      if @mm_file.update_attributes(params[:mm_file])
        format.html { redirect_to multi_model_container_primary_model_mm_content_mm_files_path, notice: 'Mm file was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @mm_file.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /mm_files/1
  # DELETE /mm_files/1.json
  def destroy
    @mm_file = MmFile.find(params[:id])
    @mm_file.destroy

    respond_to do |format|
      format.html { redirect_to multi_model_container_primary_model_mm_content_mm_files_path }
      format.json { head :no_content }
    end
  end
end
