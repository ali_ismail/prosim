class MultiModelContainersController < ApplicationController
 before_filter :authenticate_user!
  # GET /multi_model_containers
  # GET /multi_model_containers.json
  def index
    @multi_model_containers = MultiModelContainer.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @multi_model_containers }
    end
  end

  # GET /multi_model_containers/1
  # GET /multi_model_containers/1.json
  def show
    if params[:id].to_s.include?("-")
      @multi_model_container = MultiModelContainer.find_by_guid(params[:id])
    else
      @multi_model_container = MultiModelContainer.find(params[:id])
    end  

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @multi_model_container }
    end
  end

  # GET /multi_model_containers/new
  # GET /multi_model_containers/new.json
  def new
    @multi_model_container = MultiModelContainer.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @multi_model_container }
    end
  end

  # GET /multi_model_containers/1/edit
  def edit
    @multi_model_container = MultiModelContainer.find(params[:id])
  end

  # POST /multi_model_containers
  # POST /multi_model_containers.json
  def create
    @multi_model_container = MultiModelContainer.new(params[:multi_model_container])

    respond_to do |format|
      if @multi_model_container.save
        format.html { redirect_to @multi_model_container, notice: 'Multi model container was successfully created.' }
        format.json { render json: @multi_model_container, status: :created, location: @multi_model_container }
      else
        format.html { render action: "new" }
        format.json { render json: @multi_model_container.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /multi_model_containers/1
  # PUT /multi_model_containers/1.json
  def update
    @multi_model_container = MultiModelContainer.find(params[:id])

    respond_to do |format|
      if @multi_model_container.update_attributes(params[:multi_model_container])
        format.html { redirect_to @multi_model_container, notice: 'Multi model container was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @multi_model_container.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /multi_model_containers/1
  # DELETE /multi_model_containers/1.json
  def destroy
    @multi_model_container = MultiModelContainer.find(params[:id])
    @multi_model_container.destroy

    respond_to do |format|
      format.html { redirect_to multi_model_containers_url }
      format.json { head :no_content }
    end
  end
end
