class PrimaryModelsController < ApplicationController
  before_filter :authenticate_user!
  # GET /primary_models
  # GET /primary_models.json
  def index
    @mmc = MultiModelContainer.find(params[:multi_model_container_id])
    @primary_models = PrimaryModel.find_all_by_multi_model_container_id(@mmc.id)

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @primary_models }
    end
  end

  def object_models
    @mmc = MultiModelContainer.find(params[:multi_model_container_id])
    @primary_models = PrimaryModel.where(:multi_model_container_id => @mmc.id, :modeltype => "Object")
    sql = ActiveRecord::Base.connection     
    @bim_filters= sql.select_all "select name,sql from bim_filters" 
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @primary_models }
    end
    
  end
  # GET /primary_models/1
  # GET /primary_models/1.json
  def show
    @mmc = MultiModelContainer.find(params[:multi_model_container_id])
    @primary_model = PrimaryModel.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @primary_model }
    end
  end

  # GET /primary_models/new
  # GET /primary_models/new.json
  def new
    @mmc = MultiModelContainer.find(params[:multi_model_container_id])
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @primary_model }
    end
  end

  # GET /primary_models/1/edit
  def edit
    @mmc = MultiModelContainer.find(params[:multi_model_container_id])
    @primary_model = PrimaryModel.find(params[:id])
  end

  # POST /primary_models
  # POST /primary_models.json
  def create
    @mmc = MultiModelContainer.find(params[:multi_model_container_id])    
    @primary_model = @mmc.primary_models.new(params[:primary_model])
    respond_to do |format|
      if @primary_model.save
        format.html { redirect_to multi_model_container_primary_models_path, notice: 'Primary model was successfully created.' }
        format.json { render json: multi_model_container_primary_models_path, status: :created, location: @primary_model }
      else
        format.html { render action: "new" }
        format.json { render json: @primary_model.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /primary_models/1
  # PUT /primary_models/1.json
  def update
    @primary_model = PrimaryModel.find(params[:id])

    respond_to do |format|
      if @primary_model.update_attributes(params[:primary_model])
        format.html { redirect_to multi_model_container_primary_models_path, notice: 'Primary model was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @primary_model.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /primary_models/1
  # DELETE /primary_models/1.json
  def destroy
    @primary_model = PrimaryModel.find(params[:id])
    @primary_model.destroy

    respond_to do |format|
      format.html { redirect_to multi_model_container_primary_models_path }
      format.json { head :no_content }
    end
  end
end
