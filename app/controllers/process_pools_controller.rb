class ProcessPoolsController < ApplicationController
  before_filter :authenticate_user!
  # GET /process_pools
  # GET /process_pools.json
  def index
    @process_pools = ProcessPool.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @process_pools }
    end
  end

  # GET /process_pools/1
  # GET /process_pools/1.json
  def show
    @process_pool = ProcessPool.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @process_pool }
    end
  end

  # GET /process_pools/new
  # GET /process_pools/new.json
  def new
    @process_pool = ProcessPool.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @process_pool }
    end
  end

  # GET /process_pools/1/edit
  def edit
    @process_pool = ProcessPool.find(params[:id])
  end

  # POST /process_pools
  # POST /process_pools.json
  def create
    @process_pool = ProcessPool.new(params[:process_pool])

    respond_to do |format|
      if @process_pool.save
        format.html { redirect_to @process_pool, notice: 'Process pool was successfully created.' }
        format.json { render json: @process_pool, status: :created, location: @process_pool }
      else
        format.html { render action: "new" }
        format.json { render json: @process_pool.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /process_pools/1
  # PUT /process_pools/1.json
  def update
    @process_pool = ProcessPool.find(params[:id])

    respond_to do |format|
      if @process_pool.update_attributes(params[:process_pool])
        format.html { redirect_to @process_pool, notice: 'Process pool was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @process_pool.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /process_pools/1
  # DELETE /process_pools/1.json
  def destroy
    @process_pool = ProcessPool.find(params[:id])
    @process_pool.destroy

    respond_to do |format|
      format.html { redirect_to process_pools_url }
      format.json { head :no_content }
    end
  end
end
