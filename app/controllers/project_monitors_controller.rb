class ProjectMonitorsController < ApplicationController
  before_filter :authenticate_user!
  # GET /project_monitors
  # GET /project_monitors.json
  def index
    @sim_model = SimModel.find(params[:sim_model_id])
    @project_monitors =ProjectMonitor.find_all_by_sim_model_id(@sim_model.id)
    if @project_monitors.size == 1
      redirect_to "/sim_models/" + @sim_model.id.to_s + "/project_monitors/" + @project_monitors.first.id.to_s
      return
    end
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @project_monitors }
    end
  end

  # GET /project_monitors/1
  # GET /project_monitors/1.json
  def show
    @sim_model = SimModel.find(params[:sim_model_id])
    @project_monitor = ProjectMonitor.find(params[:id])
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @project_monitor }
    end
  end

  # GET /project_monitors/new
  # GET /project_monitors/new.json
  def new
    @sim_model = SimModel.find(params[:sim_model_id])
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @project_monitor }
    end
  end

  # GET /project_monitors/1/edit
  def edit
    @sim_model = SimModel.find(params[:sim_model_id])
    @project_monitor = ProjectMonitor.find(params[:id])
  end

  # POST /project_monitors
  # POST /project_monitors.json
  def create
    @sim_model = SimModel.find(params[:sim_model_id])
    @project_monitor = @sim_model.project_monitors.new(params[:project_monitor])
    respond_to do |format|
      if @project_monitor.save
        format.html { redirect_to sim_model_project_monitors_url, notice: 'Project monitor was successfully created.' }
        format.json { render json: sim_model_project_monitors_url, status: :created, location: @project_monitor }
      else
        format.html { render action: "new" }
        format.json { render json: @project_monitor.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /project_monitors/1
  # PUT /project_monitors/1.json
  def update
    @project_monitor = ProjectMonitor.find(params[:id])

    respond_to do |format|
      if @project_monitor.update_attributes(params[:project_monitor])
        format.html { redirect_to sim_model_project_monitors_url, notice: 'Project monitor was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @project_monitor.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /project_monitors/1
  # DELETE /project_monitors/1.json
  def destroy
    @project_monitor = ProjectMonitor.find(params[:id])
    @project_monitor.destroy

    respond_to do |format|
      format.html { redirect_to sim_model_project_monitors_url }
      format.json { head :no_content }
    end
  end
end
