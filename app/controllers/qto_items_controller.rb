class QtoItemsController < ApplicationController
    before_filter :authenticate_user!
  # GET /qto_items
  # GET /qto_items.json
  def index
    @search=QtoItem.search(params[:q])
    @qto_items =@search.result.paginate(:page => params[:page], :per_page => 100)  

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @qto_items }
    end
  end

  # GET /qto_items/1
  # GET /qto_items/1.json
  def show
    @qto_item = QtoItem.find_by_adresse(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @qto_item }
    end
  end

  # GET /qto_items/new
  # GET /qto_items/new.json
  def new
    @qto_item = QtoItem.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @qto_item }
    end
  end

  # GET /qto_items/1/edit
  def edit
    @qto_item = QtoItem.find_by_adresse(params[:id])
  end

  # POST /qto_items
  # POST /qto_items.json
  def create
    @qto_item = QtoItem.new(params[:qto_item])

    respond_to do |format|
      if @qto_item.save
        format.html { redirect_to @qto_item, notice: 'Qto item was successfully created.' }
        format.json { render json: @qto_item, status: :created, location: @qto_item }
      else
        format.html { render action: "new" }
        format.json { render json: @qto_item.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /qto_items/1
  # PUT /qto_items/1.json
  def update
    @qto_item = QtoItem.find(params[:id])

    respond_to do |format|
      if @qto_item.update_attributes(params[:qto_item])
        format.html { redirect_to @qto_item, notice: 'Qto item was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @qto_item.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /qto_items/1
  # DELETE /qto_items/1.json
  def destroy
    @qto_item = QtoItem.find(params[:id])
    @qto_item.destroy

    respond_to do |format|
      format.html { redirect_to qto_items_url }
      format.json { head :no_content }
    end
  end
end
