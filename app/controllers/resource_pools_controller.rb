class ResourcePoolsController < ApplicationController
  before_filter :authenticate_user!
  # GET /resource_pools
  # GET /resource_pools.json
  def index     
    @sim_model = SimModel.find(params[:sim_model_id])
    @resource_pools =ResourcePool.find_all_by_sim_model_id(@sim_model.id)
    if @resource_pools.size == 1
      redirect_to "/sim_models/" + @sim_model.id.to_s + "/resource_pools/" + @resource_pools.first.id.to_s
      return
    end
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @resource_pools }
    end
  end

  # GET /resource_pools/1
  # GET /resource_pools/1.json
  def show
    @sim_model = SimModel.find(params[:sim_model_id])    
    @resource_pool = ResourcePool.find(params[:id])
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @resource_pool }
    end
  end

  # GET /resource_pools/new
  # GET /resource_pools/new.json
  def new
    @sim_model = SimModel.find(params[:sim_model_id])
    @resource_pool = ResourcePool.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @resource_pool }
    end
  end

  # GET /resource_pools/1/edit
  def edit
    @sim_model = SimModel.find(params[:sim_model_id])    
    @resource_pool = ResourcePool.find(params[:id])
  end

  # POST /resource_pools
  # POST /resource_pools.json
  def create
    @sim_model = SimModel.find(params[:sim_model_id])    
    @resource_pool = @sim_model.resource_pools.new(params[:resource_pool])

    respond_to do |format|
      if @resource_pool.save
        format.html { redirect_to sim_model_resource_pools_url, notice: 'Resource pool was successfully created.' }
        format.json { render json: sim_model_resource_pools_url, status: :created, location: @resource_pool }
      else
        format.html { render action: "new" }
        format.json { render json: @resource_pool.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /resource_pools/1
  # PUT /resource_pools/1.json
  def update
    @resource_pool = ResourcePool.find(params[:id])
    respond_to do |format|
      if @resource_pool.update_attributes(params[:resource_pool])
        format.html { redirect_to sim_model_resource_pools_url, notice: 'Resource pool was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @resource_pool.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /resource_pools/1
  # DELETE /resource_pools/1.json
  def destroy
    @resource_pool = ResourcePool.find(params[:id])
    @resource_pool.destroy

    respond_to do |format|
      format.html { redirect_to sim_model_resource_pools_url }
      format.json { head :no_content }
    end
  end
end
