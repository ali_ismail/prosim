class RpmsController < ApplicationController
  # GET /rpms
  # GET /rpms.json
  before_filter :authenticate_user!
  def index
    @rpms = Rpm.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @rpms }
    end
  end

  # GET /rpms/1
  # GET /rpms/1.json
  def show
    @rpm = Rpm.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @rpm }
    end
  end

  # GET /rpms/new
  # GET /rpms/new.json
  def new
    @rpm = Rpm.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @rpm }
    end
  end

  # GET /rpms/1/edit
  def edit
    @rpm = Rpm.find(params[:id])
  end

  # POST /rpms
  # POST /rpms.json
  def create
    @rpm = Rpm.new(params[:rpm])
    @rpm.user_id=current_user.id
    respond_to do |format|
      if @rpm.save
        format.html { redirect_to @rpm, notice: 'Rpm was successfully created.' }
        format.json { render json: @rpm, status: :created, location: @rpm }
      else
        format.html { render action: "new" }
        format.json { render json: @rpm.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /rpms/1
  # PUT /rpms/1.json
  def update
    @rpm = Rpm.find(params[:id])

    respond_to do |format|
      if @rpm.update_attributes(params[:rpm])
        format.html { redirect_to @rpm, notice: 'Rpm was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @rpm.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /rpms/1
  # DELETE /rpms/1.json
  def destroy
    @rpm = Rpm.find(params[:id])
    @rpm.destroy

    respond_to do |format|
      format.html { redirect_to rpms_url }
      format.json { head :no_content }
    end
  end
end
