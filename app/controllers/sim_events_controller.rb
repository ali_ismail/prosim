class SimEventsController < ApplicationController
  before_filter :authenticate_user!
  # GET /sim_events
  # GET /sim_events.json  
  def index
    #@sim_events = SimEvent.all
    @search=SimEvent.search(params[:q])
    @sim_events =@search.result.paginate(:page => params[:page], :per_page => 20) 
    @search.build_condition
    respond_to do |format|
      format.html # index.html.e#rb
      format.json { render json: @sim_events }
    end
  end



  # GET /sim_events/1
  # GET /sim_events/1.json
  def show 
    if params[:id].to_s.size == 22       
       @sim_related_events = SimEvent.find_all_by_object(params[:id])
       @sim_event = SimEvent.find_by_object(params[:id])
    else
       @sim_event = SimEvent.find(params[:id])
       @sim_related_events = SimEvent.find_all_by_object(@sim_event.object)
    end  
         
    
    
   #TODO: how to get data from database tables without having a Model for this table
   # sql = ActiveRecord::Base.connection
   # @bbox= sql.select_all "select * from BBox where globalId='" + @sim_event.object + "'"   
   #  @ifc_object=Bbox.find_by_globalid(@sim_event.object)      
      scale=1.5 
      x0=500;
      y0=100;        
      $img = Rasem::SVGImage.new(750,400)
      $img.rectangle 0, 0, 750, 400,:opacity=>0.68999999000000001, :fill=>'#ececec',:fill_opacity=>0.0, :stroke=>'#31001b', :stroke_width => 0.40      
      
      
      #@ifc_object=Bbox.find(:all, :conditions => ["code like ?" ,"L=13.54m%"])
      #@ifc_object.each { |o|      
      #x1=x0+scale* o.Xmin;
      #y1=y0+ scale* o.Ymax; 
      #$img.rectangle x1,y1, scale*o.DX, scale*(o.DY) , :fill => '#0000ff'
      #}
      
      #@ifc_object=Bbox.find(:all, :conditions => ["code like ?" ,"L=38.40m%"])
      #@ifc_object.each { |o|      
      #x1=x0+scale* o.Xmin;
      #y1=y0+ scale* o.Ymax; 
      #$img.rectangle x1,y1, scale*o.DX, scale*(o.DY) , :fill => '#00ffff'
      #}
      
      @ifc_object=Bbox.find(:all, :conditions => ["code like ?" ,"rc %"])
      @ifc_object.each { |o|      
      #$img.rectangle 0, 0, 750, 400,:opacity=>0.68999999000000001, :fill=>'#ececec',:fill_opacity=>0.0, :stroke=>'#31001b', :stroke_width => 0.40
      x1=x0+scale* o.Xmin;
      y1=y0+ scale* o.Ymax; 
      $img.rectangle x1,y1, scale*o.DX, scale*(o.DY) , :fill => '#D3D3D3'
      }
      
      @ifc_object=Bbox.find(:all, :conditions => ["shape = 'circle' and dx > 120.0"])
      @ifc_object.each { |o|      
      #$img.rectangle 0, 0, 750, 400,:opacity=>0.68999999000000001, :fill=>'#ececec',:fill_opacity=>0.0, :stroke=>'#31001b', :stroke_width => 0.40
      x1=x0+scale* o.Xmin;
      y1=y0+ scale* o.Ymax; 
      $img.circle x1+o.DX,y1-o.DY*0.5, scale*o.DX*0.5 ,:opacity=>0.25,:fill=>'#fbfccd',:stroke=> '#ff0000', :stroke_width => 0.75
      }      
      
      if @sim_event != nil 
        @ifc_object=IfcObject.find(:all, :conditions => {:globalId => @sim_event.object})
        @ifc_object.each { |o|      
       x1=x0+scale* o.Xmin;
        y1=y0+ scale* o.Ymax; 
        $img.line 0,y1+scale*o.DY*0.5,750,y1+scale*o.DY*0.5,:stroke=>'#31001b', :stroke_width => 0.40
        $img.line x1+scale*o.DX*0.5,0,x1+scale*o.DX*0.5,400,:stroke=>'#31001b', :stroke_width => 0.40
        $img.rectangle x1,y1, scale*o.DX, scale*(o.DY) ,:opacity=>0.5, :fill => '#ffaaaa',:stroke=> '#ff0000', :stroke_width => 1.0
        }      
       sql = ActiveRecord::Base.connection     
       @object_atts= sql.select_all "select * from ifc_objects where globalId='" + @sim_event.object + "'"      
       else
         @no_match = true
       end 
      @svg=$img.output
     
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @sim_event }
    end
  end

  # GET /sim_events/new
  # GET /sim_events/new.json
  def new
    @sim_event = SimEvent.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @sim_event }
    end
  end

  # GET /sim_events/1/edit
  def edit
    @sim_event = SimEvent.find(params[:id])
  end

  # POST /sim_events
  # POST /sim_events.json
  def create
    @sim_event = SimEvent.new(params[:sim_event])

    respond_to do |format|
      if @sim_event.save
        format.html { redirect_to @sim_event, notice: 'Sim event was successfully created.' }
        format.json { render json: @sim_event, status: :created, location: @sim_event }
      else
        format.html { render action: "new" }
        format.json { render json: @sim_event.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /sim_events/1
  # PUT /sim_events/1.json
  def update
    @sim_event = SimEvent.find(params[:id])

    respond_to do |format|
      if @sim_event.update_attributes(params[:sim_event])
        format.html { redirect_to @sim_event, notice: 'Sim event was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @sim_event.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /sim_events/1
  # DELETE /sim_events/1.json
  def destroy
    @sim_event = SimEvent.find(params[:id])
    @sim_event.destroy

    respond_to do |format|
      format.html { redirect_to sim_events_url }
      format.json { head :no_content }
    end
  end
end
