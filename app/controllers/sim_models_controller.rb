class SimModelsController < ApplicationController
  before_filter :authenticate_user!
  # GET /sim_models
  # GET /sim_models.json
  def index
    if current_user != nil
    @sim_models = SimModel.includes(:user).where(:user_id => current_user.id).all
    else
      @sim_models = SimModel.all
     end 
    #@sim_models = SimModel.includes(:user).all
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @sim_models }
    end
  end

  # GET /sim_models/1
  # GET /sim_models/1.json
  def show
    @sim_model = SimModel.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @sim_model }
    end
  end

  # GET /sim_models/new
  # GET /sim_models/new.json
  def new
    @sim_model = SimModel.new
    @sim_model.user_id = current_user.id
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @sim_model }
    end
  end

  # GET /sim_models/1/edit
  def edit
    @sim_model = SimModel.find(params[:id])
  end

  # POST /sim_models
  # POST /sim_models.json
  def create
    @sim_model = SimModel.new(params[:sim_model])
    @sim_model.user_id = current_user.id
    respond_to do |format|
      if @sim_model.save
        format.html { redirect_to @sim_model, notice: 'Sim model was successfully created.' }
        format.json { render json: @sim_model, status: :created, location: @sim_model }
      else
        format.html { render action: "new" }
        format.json { render json: @sim_model.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /sim_models/1
  # PUT /sim_models/1.json
  def update
    @sim_model = SimModel.find(params[:id])

    respond_to do |format|
      if @sim_model.update_attributes(params[:sim_model])
        format.html { redirect_to @sim_model, notice: 'Sim model was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @sim_model.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /sim_models/1
  # DELETE /sim_models/1.json
  def destroy
    @sim_model = SimModel.find(params[:id])
    @sim_model.destroy

    respond_to do |format|
      format.html { redirect_to sim_models_url }
      format.json { head :no_content }
    end
  end
end
