class SimResourcesController < ApplicationController
  before_filter :authenticate_user!
  # GET /sim_resources
  # GET /sim_resources.json
  def index
    @resource_pool = ResourcePool.find(params[:resource_pool_id])
    @standard_resources =StandardResource.all
    @sim_resources = SimResource.find_all_by_resource_pool_id(@resource_pool.id)
    
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @sim_resources }
    end
  end

  # GET /sim_resources/1
  # GET /sim_resources/1.json
  def show
    @resource_pool= ResourcePool.find(params[:resource_pool_id])
    @standard_resources =StandardResource.all
    @sim_resource = SimResource.find(params[:id])
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @sim_resource }
    end
  end

  # GET /sim_resources/new
  # GET /sim_resources/new.json
  def new
    @resource_pool= ResourcePool.find(params[:resource_pool_id])
    @standard_resources =StandardResource.all
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @sim_resource }
    end
  end

  # GET /sim_resources/1/edit
  def edit        
    @resource_pool= ResourcePool.find(params[:resource_pool_id])
    @sim_resource = SimResource.find(params[:id])
  end

  # POST /sim_resources
  # POST /sim_resources.json
  def create    
    @resource_pool= ResourcePool.find(params[:resource_pool_id])
    @sim_resource = @resource_pool.sim_resources.new(params[:sim_resource])     
    respond_to do |format|
      if @sim_resource.save
        format.html { redirect_to sim_model_resource_pools_path, notice: 'Sim resource was successfully created.' }
        format.json { render json: @sim_resource, status: :created, location: @sim_resource }
      else
        format.html { render action: "new" }
        format.json { render json: @sim_resource.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /sim_resources/1
  # PUT /sim_resources/1.json
  def update
    @sim_resource = SimResource.find(params[:id])

    respond_to do |format|
      if @sim_resource.update_attributes(params[:sim_resource])
        format.html { redirect_to sim_model_resource_pools_path, notice: 'Sim resource was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @sim_resource.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /sim_resources/1
  # DELETE /sim_resources/1.json
  def destroy
    @sim_resource = SimResource.find(params[:id])
    @sim_resource.destroy

    respond_to do |format|
      format.html { redirect_to sim_model_resource_pools_path }
      format.json { head :no_content }
    end
  end
end
