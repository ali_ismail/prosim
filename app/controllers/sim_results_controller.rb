class SimResultsController < ApplicationController
  before_filter :authenticate_user!
  # GET /sim_results
  # GET /sim_results.json
  def index
    @sim_szenario= SimSzenario.find(params[:sim_szenario_id])
    @sim_results = SimResult.find_all_by_sim_szenario_id(@sim_szenario.id)    
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @sim_results }
    end
  end

  # GET /sim_results/1
  # GET /sim_results/1.json
  def show
    @sim_szenario= SimSzenario.find(params[:sim_szenario_id])
    @sim_result = SimResult.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @sim_result }
    end
  end

  # GET /sim_results/new
  # GET /sim_results/new.json
  def new    
    @sim_szenario= SimSzenario.find(params[:sim_szenario_id])

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @sim_result }
    end
  end

  # GET /sim_results/1/edit
  def edit
    @sim_szenario= SimSzenario.find(params[:sim_szenario_id])
    @sim_result = SimResult.find(params[:id])
  end

  # POST /sim_results
  # POST /sim_results.json
  def create    
    @sim_szenario= SimSzenario.find(params[:sim_szenario_id])    
    @sim_result = @sim_szenario.sim_results.new(params[:sim_result])    

    respond_to do |format|
      if @sim_result.save
        format.html { redirect_to sim_model_sim_szenarios_path, notice: 'Sim result was successfully created.' }
        format.json { render json: @sim_result, status: :created, location: @sim_result }
      else
        format.html { render action: "new" }
        format.json { render json: @sim_result.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /sim_results/1
  # PUT /sim_results/1.json
  def update
    @sim_result = SimResult.find(params[:id])

    respond_to do |format|
      if @sim_result.update_attributes(params[:sim_result])
        format.html { redirect_to sim_model_sim_szenarios_path, notice: 'Sim result was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @sim_result.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /sim_results/1
  # DELETE /sim_results/1.json
  def destroy
    @sim_result = SimResult.find(params[:id])
    @sim_result.destroy

    respond_to do |format|
      format.html { redirect_to sim_model_sim_szenarios_path }
      format.json { head :no_content }
    end
  end
end
