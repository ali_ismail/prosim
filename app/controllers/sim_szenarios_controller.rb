class SimSzenariosController < ApplicationController
  before_filter :authenticate_user!
  # GET /sim_szenarios
  # GET /sim_szenarios.json
  def index
    @sim_model = SimModel.find(params[:sim_model_id])
    @sim_szenarios = SimSzenario.find_all_by_sim_model_id(@sim_model.id)

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @sim_szenarios }
    end
  end

  # GET /sim_szenarios/1
  # GET /sim_szenarios/1.json
  def show
    @sim_model = SimModel.find(params[:sim_model_id])
    @sim_szenario = SimSzenario.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @sim_szenario }
    end
  end

  # GET /sim_szenarios/new
  # GET /sim_szenarios/new.json
  def new
    @sim_model = SimModel.find(params[:sim_model_id])
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @sim_szenario }
    end
  end

  # GET /sim_szenarios/1/edit
  def edit
    @sim_model = SimModel.find(params[:sim_model_id])
    @sim_szenario = SimSzenario.find(params[:id])
  end

  # POST /sim_szenarios
  # POST /sim_szenarios.json
  def create
    @sim_model = SimModel.find(params[:sim_model_id])    
    @sim_szenario = @sim_model.sim_szenarios.new(params[:sim_szenario])

    respond_to do |format|
      if @sim_szenario.save
        format.html { redirect_to sim_model_sim_szenarios_path, notice: 'Simualtion szenario was successfully created.' }
        format.json { render json: sim_model_sim_szenarios_path, status: :created, location: @sim_szenario }
      else
        format.html { render action: "new" }
        format.json { render json: @sim_szenario.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /sim_szenarios/1
  # PUT /sim_szenarios/1.json
  def update
    @sim_szenario = SimSzenario.find(params[:id])

    respond_to do |format|
      if @sim_szenario.update_attributes(params[:sim_szenario])
        format.html { redirect_to sim_model_sim_szenarios_path, notice: 'Sim szenario was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @sim_szenario.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /sim_szenarios/1
  # DELETE /sim_szenarios/1.json
  def destroy
    @sim_szenario = SimSzenario.find(params[:id])
    @sim_szenario.destroy

    respond_to do |format|
      format.html { redirect_to sim_model_sim_szenarios_path }
      format.json { head :no_content }
    end
  end
end
