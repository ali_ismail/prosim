class StandardColorsController < ApplicationController
  before_filter :authenticate_user!
  # GET /standard_colors
  # GET /standard_colors.json    
  def index
    @standard_colors = StandardColor.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @standard_colors }
      format.xml { render xml: @standard_colors  }
    end
  end

  # GET /standard_colors/1
  # GET /standard_colors/1.json
  def show
    @standard_color = StandardColor.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @standard_color }
    end
  end

  # GET /standard_colors/new
  # GET /standard_colors/new.json
  def new
    @standard_color = StandardColor.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @standard_color }
    end
  end

  # GET /standard_colors/1/edit
  def edit
    @standard_color = StandardColor.find(params[:id])
  end

  # POST /standard_colors
  # POST /standard_colors.json
  def create
    @standard_color = StandardColor.new(params[:standard_color])

    respond_to do |format|
      if @standard_color.save
        format.html { redirect_to @standard_color, notice: 'Standard color was successfully created.' }
        format.json { render json: @standard_color, status: :created, location: @standard_color }
      else
        format.html { render action: "new" }
        format.json { render json: @standard_color.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /standard_colors/1
  # PUT /standard_colors/1.json
  def update
    @standard_color = StandardColor.find(params[:id])

    respond_to do |format|
      if @standard_color.update_attributes(params[:standard_color])
        format.html { redirect_to @standard_color, notice: 'Standard color was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @standard_color.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /standard_colors/1
  # DELETE /standard_colors/1.json
  def destroy
    @standard_color = StandardColor.find(params[:id])
    @standard_color.destroy

    respond_to do |format|
      format.html { redirect_to standard_colors_url }
      format.json { head :no_content }
    end
  end
end
