class StandardResourcesController < ApplicationController
  before_filter :authenticate_user!
  # GET /standard_resources
  # GET /standard_resources.json
  def index
    @standard_resources = StandardResource.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @standard_resources }
    end
  end

  # GET /standard_resources/1
  # GET /standard_resources/1.json
  def show
    @standard_resource = StandardResource.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @standard_resource }
    end
  end

  # GET /standard_resources/new
  # GET /standard_resources/new.json
  def new
    @standard_resource = StandardResource.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @standard_resource }
    end
  end

  # GET /standard_resources/1/edit
  def edit
    @standard_resource = StandardResource.find(params[:id])
  end

  # POST /standard_resources
  # POST /standard_resources.json
  def create
    @standard_resource = StandardResource.new(params[:standard_resource])

    respond_to do |format|
      if @standard_resource.save
        format.html { redirect_to @standard_resource, notice: 'Standard resource was successfully created.' }
        format.json { render json: @standard_resource, status: :created, location: @standard_resource }
      else
        format.html { render action: "new" }
        format.json { render json: @standard_resource.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /standard_resources/1
  # PUT /standard_resources/1.json
  def update
    @standard_resource = StandardResource.find(params[:id])

    respond_to do |format|
      if @standard_resource.update_attributes(params[:standard_resource])
        format.html { redirect_to @standard_resource, notice: 'Standard resource was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @standard_resource.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /standard_resources/1
  # DELETE /standard_resources/1.json
  def destroy
    @standard_resource = StandardResource.find(params[:id])
    @standard_resource.destroy

    respond_to do |format|
      format.html { redirect_to standard_resources_url }
      format.json { head :no_content }
    end
  end
end
