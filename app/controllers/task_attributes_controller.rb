class TaskAttributesController < ApplicationController
  before_filter :authenticate_user!
  # GET /task_attributes
  # GET /task_attributes.json    
  def index
    @task_attributes = TaskAttribute.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @task_attributes }
    end
  end

  # GET /task_attributes/1
  # GET /task_attributes/1.json
  def show
    @task_attribute = TaskAttribute.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @task_attribute }
    end
  end

  # GET /task_attributes/new
  # GET /task_attributes/new.json
  def new
    @task_attribute = TaskAttribute.new	
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @task_attribute }
    end
  end

  # GET /task_attributes/1/edit
  def edit
    @task_attribute = TaskAttribute.find(params[:id])
  end

  # POST /task_attributes
  # POST /task_attributes.json
  def create  
    @task_attribute = TaskAttribute.new(params[:task_attribute])	
	@task_attribute.save
	@response = @task_attribute.errors #Errors
    @task_attribute = TaskAttribute.where(:task_id => params[:task_attribute][:task_id]).all
    @task = Task.find_by_id(params[:task_attribute][:task_id])
    respond_to do |format|
            format.js # new.html.erb
     end
  end

  # PUT /task_attributes/1
  # PUT /task_attributes/1.json
  def update
    @task_attribute = TaskAttribute.find(params[:id])
    respond_to do |format|
      if @task_attribute.update_attributes(params[:task_attribute])
        #format.html { redirect_to @task_attribute, notice: 'Task attribute was successfully updated.' }
		format.html{ redirect_to tasks_url }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @task_attribute.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /task_attributes/1
  # DELETE /task_attributes/1.json
  def destroy
    TaskAttribute.find(params[:id]).destroy
	@dtask = Task.find_by_id(params[:task_id])
    @task_attributes = @task.task_attributes if @task != nil
	 respond_to do |format|
      format.html { redirect_to task_attributes_url }
      format.json { head :no_content }
	  format.js {}
    end
	
   
  end
end
