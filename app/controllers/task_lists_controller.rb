class TaskListsController < ApplicationController
  before_filter :authenticate_user!
  # GET /task_lists
  # GET /task_lists.json
  def index    
    @sim_model = SimModel.find(params[:sim_model_id])
    @task_lists = TaskList.find_all_by_sim_model_id(@sim_model.id)
    if @task_lists.size == 1
      redirect_to "/sim_models/" + @sim_model.id.to_s + "/task_lists/" + @task_lists.first.id.to_s
      return
    end
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @task_lists }
    end
  end

  # GET /task_lists/1
  # GET /task_lists/1.json
  def show
    @sim_model = SimModel.find(params[:sim_model_id])
    @task_list = TaskList.find(params[:id])
    @task_name_filter = params[:filter]    
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @task_list }
    end
  end

  # GET /task_lists/new
  # GET /task_lists/new.json
  def new
    @sim_model = SimModel.find(params[:sim_model_id])
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @task_list }
    end
  end

  # GET /task_lists/1/edit
  def edit
    @sim_model = SimModel.find(params[:sim_model_id])
    @task_list = TaskList.find(params[:id])
  end

  # POST /task_lists
  # POST /task_lists.json
  def create
    @sim_model = SimModel.find(params[:sim_model_id])
    @task_list = @sim_model.task_lists.new(params[:task_list])

    respond_to do |format|
      if @task_list.save
        format.html { redirect_to sim_model_task_lists_path, notice: 'Task list was successfully created.' }
        format.json { render json: @task_list, status: :created, location: @task_list }
      else
        format.html { render action: "new" }
        format.json { render json: @task_list.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /task_lists/1
  # PUT /task_lists/1.json
  def update
    @task_list = TaskList.find(params[:id])

    respond_to do |format|
      if @task_list.update_attributes(params[:task_list])
        format.html { redirect_to sim_model_task_lists_path, notice: 'Task list was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @task_list.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /task_lists/1
  # DELETE /task_lists/1.json
  def destroy
    @task_list = TaskList.find(params[:id])
    @task_list.destroy

    respond_to do |format|
      format.html { redirect_to sim_model_task_lists_path }
      format.json { head :no_content }
    end
  end
end
