class TasksController < ApplicationController
  before_filter :authenticate_user!
  # GET /tasks
  # GET /tasks.json  
  #caches_action  :index,:layout => false , :cache_path => Proc.new {|c| c.request.url }
  def index
    
    @task_list = TaskList.find(params[:task_list_id])
    @tasks = Task.includes(:task_attributes).order(:start_date,:ProcessPool).find_all_by_task_list_id(@task_list.id)

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @tasks }
	  format.xml { render xml: @tasks }
    end
  end

  # GET /tasks/1
  # GET /tasks/1.json
  def show
    @task_list = TaskList.find(params[:task_list_id])
    @task = Task.find_by_id(params[:id])
	  @task_attributes=@task.task_attributes
	  @title=Task.name
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @task }
    end	
  end

  # GET /tasks/new
  # GET /tasks/new.json
  def new
    @task_list = TaskList.find(params[:task_list_id])    

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @task }
    end
  end

  # GET /tasks/1/edit
  def edit
    expire_action :action => :index
    @task_list = TaskList.find(params[:task_list_id])
    @task = Task.find_by_id(params[:id])
  	@task_attributes=@task.task_attributes
  	@title=Task.name
  	@task_attribute=TaskAttribute.new
  end

  # POST /tasks
  # POST /tasks.json
  def create
    expire_action :action => :index
    @task_list = TaskList.find(params[:task_list_id])
    @task =@task_list.tasks.new(params[:task])
    respond_to do |format|
      if @task.save
        format.html { redirect_to sim_model_task_list_tasks_path, notice: 'Task was successfully created.' }
        format.json { render json: @task, status: :created, location: @task }
      else
        format.html { render action: "new" }
        format.json { render json: @task.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /tasks/1
  # PUT /tasks/1.json
  def update
    @task = Task.find_by_id(params[:id])

    respond_to do |format|
      if @task.update_attributes(params[:task])
        format.html { redirect_to sim_model_task_list_tasks_path, notice: 'Task was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @task.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /tasks/1
  # DELETE /tasks/1.json
  def destroy
    @task = Task.find_by_id(params[:id])
    @task.destroy
    respond_to do |format|
      format.html { redirect_to sim_model_task_list_tasks_path }
      format.json { head :no_content }
    end
  end
end
