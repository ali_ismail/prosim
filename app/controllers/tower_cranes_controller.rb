class TowerCranesController < ApplicationController
    before_filter :authenticate_user!
  # GET /tower_cranes
  # GET /tower_cranes.json
  def index
    @resource_pool = ResourcePool.find(params[:resource_pool_id])
    @tower_cranes = TowerCrane.find_all_by_resource_pool_id(@resource_pool.id)

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @tower_cranes }
    end
  end

  # GET /tower_cranes/1
  # GET /tower_cranes/1.json
  def show
    @resource_pool= ResourcePool.find(params[:resource_pool_id])
    @tower_crane = TowerCrane.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @tower_crane }
    end
  end

  # GET /tower_cranes/new
  # GET /tower_cranes/new.json
  def new
    @resource_pool= ResourcePool.find(params[:resource_pool_id])
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @tower_crane }
    end
  end

  # GET /tower_cranes/1/edit
  def edit
    @resource_pool= ResourcePool.find(params[:resource_pool_id])
    @tower_crane = TowerCrane.find(params[:id])
  end

  # POST /tower_cranes
  # POST /tower_cranes.json
  def create
    @resource_pool= ResourcePool.find(params[:resource_pool_id])
    @tower_crane = @resource_pool.tower_cranes.new(params[:tower_crane])    

    respond_to do |format|
      if @tower_crane.save
        format.html { redirect_to sim_model_resource_pools_path, notice: 'Tower crane was successfully created.' }
        format.json { render json: @tower_crane, status: :created, location: @tower_crane }
      else
        format.html { render action: "new" }
        format.json { render json: @tower_crane.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /tower_cranes/1
  # PUT /tower_cranes/1.json
  def update
    @tower_crane = TowerCrane.find(params[:id])

    respond_to do |format|
      if @tower_crane.update_attributes(params[:tower_crane])
        format.html { redirect_to sim_model_resource_pools_path, notice: 'Tower crane was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @tower_crane.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /tower_cranes/1
  # DELETE /tower_cranes/1.json
  def destroy
    @tower_crane = TowerCrane.find(params[:id])
    @tower_crane.destroy

    respond_to do |format|
      format.html { redirect_to sim_model_resource_pools_path }
      format.json { head :no_content }
    end
  end
end
