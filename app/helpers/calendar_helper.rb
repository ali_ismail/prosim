module CalendarHelper
  def month_link(month_date)
    link_to(I18n.localize(month_date, :format => "%B"), {:month => month_date.month, :year => month_date.year})
  end
  
  # custom options for this calendar
  def event_calendar_opts
    { 
      :year => @year,
      :month => @month,  
      :event_height => 18,
      :event_margin => 1,
      :event_padding_top => 0,
      :event_height => 10,
      :event_strips => @event_strips,
      :first_day_of_week => 0,
      :month_name_text => I18n.localize(@shown_month, :format => "%B %Y"),
      :previous_month_text => "<< " + month_link(@shown_month.prev_month),
      :next_month_text => month_link(@shown_month.next_month) + " >>"    }
  end

  def event_calendar
    # args is an argument hash containing :event, :day, and :options
    calendar event_calendar_opts do |args|    
      event = args[:event]
      #%(<a href="/sim_events/#{event.id}" title="#{h(event.name)}">#{h(event.name)}</a>)
      #%(<a href="/sim_events/#{event.id}" title="#{h(event.name)}">#{h(event.id)}</a>)
      #%(<a href="/sim_events/#{event.id}" title="#{h(event.name)}">?</a>")
       %(<a href="/sim_events/#{event.id}" title="#{h(event.name)}" class="screenshot" rel="#{event.sim_szenario.sim_model.bim_url}/png/#{event.object}.png" >?</a>)
    end
  end
end
