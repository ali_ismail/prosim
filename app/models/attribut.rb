class Attribut < ActiveRecord::Base
  default_scope order("name")
  attr_accessible :attType, :dflt_value, :name, :notnull
end
