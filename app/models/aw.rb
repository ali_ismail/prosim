class Aw < ActiveRecord::Base
  belongs_to :sim_szenario
  attr_accessible :arbeitsweise, :bauteil, :bauweise, :changed_by, :hinweis, :menge_einheit, :name, :pro, :spezifizierung, :value, :zeit_einheit
    # Prevent creation of new records and modification to existing records
  def readonly?
    return false
  end
 
  # Prevent objects from being destroyed
  def before_destroy    
    raise ActiveRecord::ReadOnlyRecord
  end
  
  def self.delete_all
    raise ActiveRecord::ReadOnlyRecord
  end
  
  def delete
    raise ActiveRecord::ReadOnlyRecord
  end
end
