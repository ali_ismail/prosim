class Bbox < ActiveRecord::Base
  attr_accessible :Code, :DX, :DY, :DZ, :ElementClass, :LineWidth, :Xmax, :Xmin, :Ymax, :Ymin, :Zmax, :Zmin, :default_crane, :globalid, :line_id, :rotation, :shape
end
