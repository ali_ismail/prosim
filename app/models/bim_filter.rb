class BimFilter < ActiveRecord::Base
  default_scope order("name")
  attr_accessible :name, :sql
end
