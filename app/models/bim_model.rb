class BimModel < ActiveRecord::Base
  belongs_to :sim_model
  attr_accessible :bim_path, :bim_site, :ifc_classes, :name
end
