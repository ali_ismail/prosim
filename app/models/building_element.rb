class BuildingElement < ActiveRecord::Base
  belongs_to :primary_model
  attr_accessible :depth, :description, :globalid, :ifc_class, :ifc_line_id, :length, :level, :material, :name, :objectType, :revit_id, :tag, :volume, :width, :work_section
end
