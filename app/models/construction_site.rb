class ConstructionSite < ActiveRecord::Base
  belongs_to :sim_model
  belongs_to :task_list
  attr_accessible :sim_model_id, :task_list_id
end
