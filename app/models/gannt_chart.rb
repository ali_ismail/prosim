class GanntChart < ActiveRecord::Base
  belongs_to :sim_model
  attr_accessible :name
  # Prevent creation of new records and modification to existing records
  def readonly?
    return true
  end
 
  # Prevent objects from being destroyed
  def before_destroy
    
    raise ActiveRecord::ReadOnlyRecord
  end
  
  def self.delete_all
    raise ActiveRecord::ReadOnlyRecord
  end
  
  def delete
    raise ActiveRecord::ReadOnlyRecord
  end
end
