class Link < ActiveRecord::Base
  belongs_to :link_model
  attr_accessible :m1,:m2,:m3,:m4,:m5,:m6,:m7,:m8,:m9,:m10
  attr_accessible :m1_namespace,:m2_namespace,:m3_namespace,:m4_namespace,:m5_namespace,:m6_namespace,:m7_namespace,:m8_namespace,:m9_namespace,:m10_namespace
  attr_accessible :m1_c,:m2_c,:m3_c,:m4_c,:m5_c,:m6_c,:m7_c,:m8_c,:m9_c,:m10_c 
end
