class LinkModel < ActiveRecord::Base
  belongs_to :multi_model_container   
  has_many :linking_tables
  has_many :primary_models , :through => :linking_tables
  has_many :links
  
  attr_accessible :file, :linkmodelid, :linkmodeltype
end
