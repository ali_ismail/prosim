class LinkingTable < ActiveRecord::Base
  belongs_to :link_model
  belongs_to :primary_model  
  attr_accessible :link_model_id, :primary_model_id
  
end
