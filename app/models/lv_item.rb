class LvItem < ActiveRecord::Base
  attr_accessible :building_element_class, :color, :default_calander, :description_CompleteText, :description_OutlineText, :drawResourcesScript, :duration_formula, :export3D, :it, :item_id, :name, :onEndScript
  attr_accessible :onStartScript, :predQty, :priority, :qty, :qu, :quantity_formula, :rNOpaer, :resources, :up
  
  
  def readonly?
    return false
  end
 
  # Prevent objects from being destroyed
  def before_destroy    
    raise ActiveRecord::ReadOnlyRecord
  end
  
  def self.delete_all
    raise ActiveRecord::ReadOnlyRecord
  end
  
  def delete
    raise ActiveRecord::ReadOnlyRecord
  end
end
