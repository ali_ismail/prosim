class MmContent < ActiveRecord::Base
  belongs_to :primary_model
  has_many :mm_files
  attr_accessible :contentid, :format, :formatVersion
end
