class MmFile < ActiveRecord::Base
  belongs_to :mm_content
  attr_accessible :file, :namespace
end
