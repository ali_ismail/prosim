class MultiModelContainer < ActiveRecord::Base
  has_many :primary_models
  has_many :link_models  
  attr_accessible :formatVersion, :guid, :meta_orgin_appVersion, :meta_orgin_application, :meta_orgin_created, :meta_orgin_creatorId, :mmc_file_path, :mmc_folder_path, :name
  
   # Prevent creation of new records and modification to existing records
  def readonly?
    return false
  end
 
  # Prevent objects from being destroyed
  def before_destroy
    
    raise ActiveRecord::ReadOnlyRecord
  end
  
  def self.delete_all
    raise ActiveRecord::ReadOnlyRecord
  end
  
  def delete
    raise ActiveRecord::ReadOnlyRecord
  end
end
