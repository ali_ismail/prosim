class PrimaryModel < ActiveRecord::Base
  belongs_to :multi_model_container
  has_many :mm_contents
  has_many :linking_tables
  has_many :link_models , :through => :linking_tables
  attr_accessible :contextId, :modelType, :modelid
end
