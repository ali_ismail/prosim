class ProcessPool < ActiveRecord::Base
  belongs_to :sim_model
  attr_accessible :orgchart, :sim_model_id
end
