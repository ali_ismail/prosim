class ProjectMonitor < ActiveRecord::Base
  belongs_to :sim_model
  attr_accessible :name, :sim_model_id
end
