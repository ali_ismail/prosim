class QtoItem < ActiveRecord::Base
  belongs_to :building_element
  belongs_to :primary_model
  attr_accessible :adresse, :formelnummer, :ort, :oz, :rechenansatz, :result
  
  def to_param
     self.adresse
  end 
  
  def self.from_param(param)
    find_by_adresse!(param)
  end
end
