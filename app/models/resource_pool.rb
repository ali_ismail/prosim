class ResourcePool < ActiveRecord::Base
  belongs_to :sim_model
  has_many :tower_cranes
  has_many :sim_resources
  attr_accessible :name, :sim_model_id
end
