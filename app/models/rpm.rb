class Rpm < ActiveRecord::Base
  belongs_to :sim_model
  belongs_to :user
  attr_accessible :bpmn_path, :description, :img_path, :name, :tree_path
  
    
  # Prevent creation of new records and modification to existing records
  def readonly?
    return false
  end
 
  # Prevent objects from being destroyed
  def before_destroy
    
    raise ActiveRecord::ReadOnlyRecord
  end
  
  def self.delete_all
    raise ActiveRecord::ReadOnlyRecord
  end
  
  def delete
    raise ActiveRecord::ReadOnlyRecord
  end
end
