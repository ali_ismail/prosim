class SimEvent < ActiveRecord::Base
  has_event_calendar :start_at_field  => 'eventdate', :end_at_field => 'eventdate'
  attr_accessible :changed_at, :obj_status, :object ,:eventtype,:eventtime,:eventdate
  belongs_to :sim_szenario
  def name
    self.eventtype + " of " + self.obj_status
  end
  
  def color
    alpha="0.3" if self.eventtype == "start"
    alpha="1.0" if self.eventtype == "end"
    return "RGBA(0,0,0," + alpha + ")"
    #return "RGBA(" + StandardColor.first.R.to_s + "," + StandardColor.first.G.to_s + "," + StandardColor.first.B.to_s + "," + alpha + ")"
  end  
end