class SimModel < ActiveRecord::Base
  belongs_to :user   
  has_many :process_pools 
  has_many :sim_events 
  has_many :rpms
  has_many :task_lists
  has_many :sim_resources
  has_many :draw_panels
  has_many :project_monitors
  has_many :tower_cranes
  has_many :resource_pools
  has_many :gannt_charts
  has_many :sim_szenarios  
  has_many :construction_sites 
  attr_accessible :description, :model_tree_path, :name, :results_url, :user_id, :bim_url  
  
  # Prevent creation of new records and modification to existing records
  def readonly?    
    return false
  end
 
  # Prevent objects from being destroyed
  def before_destroy
    
    raise ActiveRecord::ReadOnlyRecord
  end
  
  def self.delete_all
    raise ActiveRecord::ReadOnlyRecord
  end
  
  def delete
    raise ActiveRecord::ReadOnlyRecord
  end
end
