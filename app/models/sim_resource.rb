class SimResource < ActiveRecord::Base
  belongs_to :resource_pool
  attr_accessible :delivery_date, :delivery_time, :name, :res_class, :res_num, :resource_pool_id
end
