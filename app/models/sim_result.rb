class SimResult < ActiveRecord::Base
  belongs_to :sim_szenario
  attr_accessible :description, :name, :result_type, :result_url
end
