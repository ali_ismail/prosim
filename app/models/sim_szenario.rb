class SimSzenario < ActiveRecord::Base
  belongs_to :sim_model
  has_many :sim_results
  has_many :sim_events 
  attr_accessible :description, :name
end
