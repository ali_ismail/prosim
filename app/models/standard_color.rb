class StandardColor < ActiveRecord::Base
  default_scope order("name")
  attr_accessible :A, :B, :G, :R, :RGB, :name, :name_de
end
