class StandardResource < ActiveRecord::Base
  attr_accessible :category, :description, :name, :parent_id
end
