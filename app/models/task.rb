class Task < ActiveRecord::Base
  has_many :task_attributes
  belongs_to :task_list
  after_initialize :init
  after_save :save_me  
  attr_accessible :ProcessPool, :buildingElement_id, :duration, :duration_formula, :globalId, :name, :predecessor, :predecessor_puffer, :processTemplate, :start_date,:end_date, :start_time, :task_numOfInstances
  
  
   def init
    self.task_numOfInstances  ||= 1.0 
	  self.processTemplate  ||= ".Mefisto.Prozesse.Task"
   end  

	
   def save_me    
	   if task_attributes.size == 0
		 task_attributes.create("AttName" => "ProcessTemplate","AttValue" => ".Mefisto.Prozesse.Task","AttType" => "string")
		 task_attributes.create("AttName" => "ProcessPool","AttValue" => "","AttType" => "string")
		 task_attributes.create("AttName" => "Duration","AttValue" => "","AttType" => "time")
		 task_attributes.create("AttName" => "Duration_min","AttValue" => "","AttType" => "time")
		 task_attributes.create("AttName" => "Duration_max","AttValue" => "","AttType" => "time")
		 task_attributes.create("AttName" => "Duration_formula","AttValue" => "","AttType" => "time")
		 task_attributes.create("AttName" => "Predecessor","AttValue" => "","AttType" => "string")
		 task_attributes.create("AttName" => "globalid","AttValue" => "","AttType" => "string")
		 task_attributes.create("AttName" => "BuildingElement_ID","AttValue" => "","AttType" => "integer")	 
		end 
   end
   
end
