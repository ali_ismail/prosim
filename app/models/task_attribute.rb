class TaskAttribute < ActiveRecord::Base
  belongs_to :task
  attr_accessible :AttName, :AttType, :AttValue , :task_id , :prerequisite_attributes
end
