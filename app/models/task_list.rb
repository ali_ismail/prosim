class TaskList < ActiveRecord::Base
  belongs_to :sim_model
  has_many :tasks
  attr_accessible :name, :sim_model_id, :source_path
end
