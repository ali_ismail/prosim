ProSim::Application.routes.draw do
   
  get "cst_doc/index"

  resources :bim_models

  #resources :construction_sites

  resources :aws

   match '/ifc_objects/view' => 'ifc_objects#svg_viewer'
   match '/ifc_objects/count_by_code' => 'ifc_objects#count_by_code'
   match '/ifc_objects/count_by_code_by_floor' => 'ifc_objects#count_by_code_by_floor'
   resources :ifc_objects   
   match '/mmc' => 'multi_model_containers#index'
   match '/cache' => 'draw_panels#show_all'
    match '/multi_model_containers/:multi_model_container_id/primary_models/object' => 'primary_models#object_models'
    resources :multi_model_containers do
    resources :link_models
    resources :primary_models do
    resources :mm_contents do
    resources :mm_files    
    end    
    end  
  end

  
      
  resources :sim_models do
    resources :project_monitors
    resources :construction_sites
    resources :draw_panels
    resources :task_lists do
      resources :tasks  do
        resources :task_attributes , :only =>[:create, :destroy]
      end
    end
    resources :process_pools
    resources :resource_pools do
      resources :sim_resources
      resources :tower_cranes
    end
    resources :gannt_charts
    resources :sim_szenarios do
      resources :sim_results
    end   
  end
  match '/calendar(/:year(/:month))' => 'calendar#index', :as => :calendar, :constraints => {:year => /\d{4}/, :month => /\d{1,2}/}
  devise_for :users  
  resources :rpms
  resources :sim_events , :only =>[:index]
  resources :events_reports, :only =>[:index]
  resources :sim_events do
    collection {post :search, to: 'sim_events#index'}
  end
  
  resources :standard_resources
  resources :qto_items
  resources :building_elements
  resources :links
  resources :lv_items  
  resources :attributs
  resources :standard_colors
  resources :bim_filters    
  resources :task_attributes
  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  root :to => 'welcome#index'
  
  
  

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id))(.:format)'
end
