class CreateBimFilters < ActiveRecord::Migration
  def change
    create_table :bim_filters do |t|
      t.string :name
      t.string :sql

      t.timestamps
    end
  end
end
