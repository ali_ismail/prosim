class CreateStandardColors < ActiveRecord::Migration
  def change
    create_table :standard_colors do |t|
      t.integer :R
      t.integer :G
      t.integer :B
      t.float :A
      t.integer :RGB
      t.string :name
      t.string :name_de

      t.timestamps
    end
  end
end
