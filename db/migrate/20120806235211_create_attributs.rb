class CreateAttributs < ActiveRecord::Migration
  def change
    create_table :attributs do |t|
      t.string :name
      t.string :attType
      t.boolean :notnull
      t.string :dflt_value

      t.timestamps
    end
  end
end
