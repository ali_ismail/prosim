class CreateTasks < ActiveRecord::Migration
  def change
    create_table :tasks do |t|
      t.float :start_time
      t.datetime :start_date
      t.integer :task_numOfInstances
      t.string :name
      t.string :processTemplate
      t.string :ProcessPool
      t.string :duration
      t.string :duration_formula
      t.string :predecessor
      t.float :predecessor_puffer
      t.string :globalId
      t.integer :buildingElement_id
      t.timestamps
    end
  end
end
