class CreateTaskAttributes < ActiveRecord::Migration
  def change
    create_table :task_attributes do |t|
      t.string :AttName
      t.string :AttValue
      t.string :AttType

      t.timestamps
    end
  end
end
