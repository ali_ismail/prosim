class AddTaskIdToTaskAttribute < ActiveRecord::Migration
  def change
    add_column :task_attributes, :task_id, :integer
  end
end
