class CreateSimEvents < ActiveRecord::Migration
  def change
    create_table :sim_events do |t|
      t.string :object
      t.string :obj_status
      t.float :changed_at
    end
  end
end
