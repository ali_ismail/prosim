class CreateRpms < ActiveRecord::Migration
  def change
    create_table :rpms do |t|
      t.string :name
      t.string :description
      t.string :tree_path
      t.string :bpmn_path
      t.string :img_path
      t.integer :user_id

      t.timestamps
    end
  end
end
