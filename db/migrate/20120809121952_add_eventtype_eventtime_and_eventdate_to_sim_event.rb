#rails g migration add_eventtype_eventtime_and_eventdate_to_sim_event eventtype:string  eventtime:string eventdate:datetime
class AddEventtypeEventtimeAndEventdateToSimEvent < ActiveRecord::Migration
  def change
    add_column :sim_events, :eventtype, :string
    add_column :sim_events, :eventtime, :string
    add_column :sim_events, :eventdate, :datetime
  end
end
