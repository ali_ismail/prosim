class AddColorToSimEvent < ActiveRecord::Migration
  def change
    add_column :sim_events, :color, :string
  end
end
