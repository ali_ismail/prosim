class CreateSimModels < ActiveRecord::Migration
  def change
    create_table :sim_models do |t|
      t.string :name
      t.integer :user_id
      t.text :description
      t.string :model_tree_path
      t.string :results_url

      t.timestamps
    end
  end
end
