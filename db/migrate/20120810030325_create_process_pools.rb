class CreateProcessPools < ActiveRecord::Migration
  def change
    create_table :process_pools do |t|
      t.integer :sim_model_id
      t.text :orgchart

      t.timestamps
    end
  end
end
