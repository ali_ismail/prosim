class AddSimModelIdToSimEvent < ActiveRecord::Migration
  def change
    add_column :sim_events, :sim_model_id, :integer
  end
end
