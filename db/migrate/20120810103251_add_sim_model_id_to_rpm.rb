class AddSimModelIdToRpm < ActiveRecord::Migration
  def change
    add_column :rpms, :sim_model_id, :integer
  end
end
