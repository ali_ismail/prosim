class AddSimModelIdToTask < ActiveRecord::Migration
  def change
    add_column :tasks, :sim_model_id, :integer
  end
end
