class CreateTaskLists < ActiveRecord::Migration
  def change
    create_table :task_lists do |t|
      t.string :name
      t.integer :sim_model_id
      t.string :source_path

      t.timestamps
    end
  end
end
