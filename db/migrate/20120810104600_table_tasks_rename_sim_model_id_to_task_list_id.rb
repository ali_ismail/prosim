class TableTasksRenameSimModelIdToTaskListId < ActiveRecord::Migration
  def up
  rename_column :tasks, :sim_model_id, :task_list_id
  end

  def down
  end
end
