class CreateSimResources < ActiveRecord::Migration
  def change
    create_table :sim_resources do |t|
      t.string :name
      t.string :res_class
      t.float :res_num
      t.float :delivery_time
      t.datetime :delivery_date
      t.integer :sim_model_id

      t.timestamps
    end
  end
end
