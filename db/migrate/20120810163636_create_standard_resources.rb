class CreateStandardResources < ActiveRecord::Migration
  def change
    create_table :standard_resources do |t|
      t.string :name
      t.string :description
      t.integer :parent_id
      t.string :category

      t.timestamps
    end
  end
end
