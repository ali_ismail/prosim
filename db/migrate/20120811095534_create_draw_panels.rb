class CreateDrawPanels < ActiveRecord::Migration
  def change
    create_table :draw_panels do |t|
      t.string :name
      t.string :sim_model_id

      t.timestamps
    end
  end
end
