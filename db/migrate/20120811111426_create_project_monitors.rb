class CreateProjectMonitors < ActiveRecord::Migration
  def change
    create_table :project_monitors do |t|
      t.string :name
      t.string :sim_model_id

      t.timestamps
    end
  end
end
