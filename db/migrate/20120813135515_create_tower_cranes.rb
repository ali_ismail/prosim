class CreateTowerCranes < ActiveRecord::Migration
  def change
    create_table :tower_cranes do |t|
      t.string :name
      t.string :craneModel
      t.float :rotation_speed
      t.float :trolly_speed
      t.float :z_speed
      t.integer :draw_layer
      t.string :resource_pool
      t.string :obj_class
      t.string :sim_obj_path
      t.float :xPos
      t.float :yPos
      t.float :raduis
      t.float :height
      t.float :baseWidth
      t.time :start_time
      t.time :end_time
      t.datetime :start_date
      t.datetime :end_date

      t.timestamps
    end
  end
end
