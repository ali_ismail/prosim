class AddsimModelIdToTowerCrane < ActiveRecord::Migration
  def change
    add_column :tower_cranes , :sim_model_id, :integer
  end
end
