class CreateResourcePools < ActiveRecord::Migration
  def change
    create_table :resource_pools do |t|
      t.string :name
      t.integer :sim_model_id

      t.timestamps
    end
  end
end
