class CreateGanntCharts < ActiveRecord::Migration
  def change
    create_table :gannt_charts do |t|
      t.string :name
      t.references :sim_model

      t.timestamps
    end
    add_index :gannt_charts, :sim_model_id
  end
end
