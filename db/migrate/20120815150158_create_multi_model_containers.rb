class CreateMultiModelContainers < ActiveRecord::Migration
  def change
    create_table :multi_model_containers do |t|
      t.string :name
      t.string :mmc_file_path
      t.string :mmc_folder_path
      t.datetime :meta_orgin_created
      t.string :meta_orgin_creatorId
      t.string :meta_orgin_application
      t.string :meta_orgin_appVersion
      t.string :guid
      t.string :formatVersion

      t.timestamps
    end
  end
end
