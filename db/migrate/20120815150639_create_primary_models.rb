class CreatePrimaryModels < ActiveRecord::Migration
  def change
    create_table :primary_models do |t|
      t.string :modelType
      t.string :modelid
      t.string :contextId
      t.references :multi_model_container

      t.timestamps
    end
    add_index :primary_models, :multi_model_container_id
  end
end
