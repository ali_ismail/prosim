class CreateMmContents < ActiveRecord::Migration
  def change
    create_table :mm_contents do |t|
      t.string :format
      t.string :contentid
      t.string :formatVersion
      t.references :primary_model

      t.timestamps
    end
    add_index :mm_contents, :primary_model_id
  end
end
