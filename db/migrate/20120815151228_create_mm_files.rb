class CreateMmFiles < ActiveRecord::Migration
  def change
    create_table :mm_files do |t|
      t.string :namespace
      t.string :file
      t.references :mm_content

      t.timestamps
    end
    add_index :mm_files, :mm_content_id
  end
end
