class CreateLinkModels < ActiveRecord::Migration
  def change
    create_table :link_models do |t|
      t.string :linkmodeltype
      t.string :linkmodelid
      t.string :file
      t.references :multi_model_container

      t.timestamps
    end
    add_index :link_models, :multi_model_container_id
  end
end
