class CreateLinkingTables < ActiveRecord::Migration
  def change
    create_table :linking_tables do |t|
      t.integer :link_model_id
      t.integer :primary_model_id

      t.timestamps
    end
  end
end
