class AddmmcIdToSimModel < ActiveRecord::Migration
  def change
    add_column :sim_models, :multi_model_container_id, :integer
  end
end
