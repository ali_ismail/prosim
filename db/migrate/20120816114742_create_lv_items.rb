class CreateLvItems < ActiveRecord::Migration
  def change
    create_table :lv_items do |t|
      t.string :item_id
      t.string :name
      t.string :rNOpaer
      t.float :qty
      t.float :predQty
      t.string :qu
      t.float :up
      t.float :it
      t.text :description_CompleteText
      t.string :description_OutlineText
      t.float :priority
      t.string :building_element_class
      t.string :default_calander
      t.string :duration_formula
      t.text :resources
      t.text :onStartScript
      t.text :onEndScript
      t.text :drawResourcesScript
      t.string :color
      t.boolean :export3D
      t.text :quantity_formula

      t.timestamps
    end
  end
end
