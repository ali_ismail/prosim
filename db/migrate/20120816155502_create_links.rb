class CreateLinks < ActiveRecord::Migration
  def change
    create_table :links do |t|
	  t.string :m1 ,:limit => 255
	  t.string :m2 ,:limit => 255
	  t.string :m3 ,:limit => 255
	  t.string :m4 ,:limit => 255
	  t.string :m5 ,:limit => 255 
	  t.string :m6 ,:limit => 255
      t.string :m7 ,:limit => 255
	  t.string :m8 ,:limit => 255
	  t.string :m9  ,:limit => 255
	  t.string :m10	,:limit => 255
	  t.string :m1_namespace ,:limit => 40
	  t.string :m2_namespace ,:limit => 40
	  t.string :m3_namespace ,:limit => 40
	  t.string :m4_namespace ,:limit => 40
	  t.string :m5_namespace ,:limit => 40
	  t.string :m6_namespace ,:limit => 40
      t.string :m7_namespace ,:limit => 40
	  t.string :m8_namespace ,:limit => 40
	  t.string :m9_namespace ,:limit => 40
	  t.string :m10_namespace ,:limit => 40
	  t.integer :m1_c
	  t.integer :m2_c
	  t.integer :m3_c
	  t.integer :m4_c
	  t.integer :m5_c
	  t.integer :m6_c
      t.integer :m7_c
	  t.integer :m8_c
	  t.integer :m9_c
	  t.integer :m10_c       
	  t.references :link_model	  
    end
	add_index :links, :link_model_id
  end
end
