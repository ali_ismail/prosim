class CreateBuildingElements < ActiveRecord::Migration
  def change
    create_table :building_elements do |t|
      t.string :globalid
      t.string :name
      t.string :ifc_class
      t.string :objectType
      t.string :ifc_line_id
      t.string :tag
      t.string :description
      t.string :level
      t.string :work_section
      t.string :material
      t.string :revit_id
      t.float :volume
      t.float :length
      t.float :depth
      t.float :width
      t.references :primary_model

      t.timestamps
    end
    add_index :building_elements, :primary_model_id
  end
end
