class CreateQtoItems < ActiveRecord::Migration
  def change
    create_table :qto_items do |t|
      t.string :oz
      t.string :ort
      t.string :adresse
      t.string :formelnummer
      t.float :result
      t.string :rechenansatz
      t.references :building_element
      t.references :primary_model


    end
    add_index :qto_items, :building_element_id
    add_index :qto_items, :primary_model_id
  end
end
