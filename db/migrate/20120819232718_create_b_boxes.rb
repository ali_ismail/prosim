class CreateBBoxes < ActiveRecord::Migration
  def change
    create_table :bboxes do |t|
      t.string :globalid
      t.integer :line_id
      t.string :ElementClass
      t.float :Xmin
      t.float :Xmax
      t.float :Ymin
      t.float :Ymax
      t.float :Zmin
      t.float :Zmax
      t.float :DX
      t.float :DY
      t.float :DZ
      t.string :Code
      t.string :default_crane
      t.string :shape
      t.string :LineWidth
      t.float :rotation
    end
  end
end
