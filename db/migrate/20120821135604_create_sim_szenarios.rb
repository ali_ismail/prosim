class CreateSimSzenarios < ActiveRecord::Migration
  def change
    create_table :sim_szenarios do |t|
      t.string :name
      t.text :description
      t.references :sim_model

      t.timestamps
    end
    add_index :sim_szenarios, :sim_model_id
  end
end
