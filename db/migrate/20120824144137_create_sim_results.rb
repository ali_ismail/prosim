class CreateSimResults < ActiveRecord::Migration
  def change
    create_table :sim_results do |t|
      t.string :name
      t.string :result_type
      t.string :result_url
      t.text :description
      t.references :sim_szenario

      t.timestamps
    end
    add_index :sim_results, :sim_szenario_id
  end
end
