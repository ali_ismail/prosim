class CreateAws < ActiveRecord::Migration
  def change
    create_table :aws do |t|
      t.string :bauweise
      t.string :arbeitsweise
      t.string :bauteil
      t.string :spezifizierung
      t.string :name
      t.float :value
      t.string :menge_einheit
      t.string :zeit_einheit
      t.string :pro
      t.text :hinweis
      t.references :sim_szenario
      t.integer :changed_by

      t.timestamps
    end
    add_index :aws, :sim_szenario_id
  end
end
