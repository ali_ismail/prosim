class FixColumnName < ActiveRecord::Migration
  def change
    rename_column :sim_resources, :sim_model_id, :resource_pool_id
  end 
end
