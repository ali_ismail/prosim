class CreateConstructionSites < ActiveRecord::Migration
  def change
    create_table :construction_sites do |t|
      t.references :sim_model
      t.references :task_list
    end
    add_index :construction_sites, :sim_model_id
    add_index :construction_sites, :task_list_id
  end
end
