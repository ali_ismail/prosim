class CreateBimModels < ActiveRecord::Migration
  def change
    create_table :bim_models do |t|
      t.string :name
      t.string :bim_path
      t.float :bim_site
      t.text :ifc_classes
      t.references :sim_model

      t.timestamps
    end
    add_index :bim_models, :sim_model_id
  end
end
