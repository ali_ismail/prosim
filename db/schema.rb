# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20120910135158) do

  create_table "attributs", :force => true do |t|
    t.string   "name"
    t.string   "attType"
    t.boolean  "notnull"
    t.string   "dflt_value"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "aws", :force => true do |t|
    t.string   "bauweise"
    t.string   "arbeitsweise"
    t.string   "bauteil"
    t.string   "spezifizierung"
    t.string   "name"
    t.float    "value"
    t.string   "menge_einheit"
    t.string   "zeit_einheit"
    t.string   "pro"
    t.text     "hinweis"
    t.integer  "sim_szenario_id"
    t.integer  "changed_by"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

  add_index "aws", ["sim_szenario_id"], :name => "index_aws_on_sim_szenario_id"

# Could not dump table "bboxes" because of following StandardError
#   Unknown type 'REAL' for column 'Xmin'

  create_table "bim_filters", :force => true do |t|
    t.string   "name"
    t.string   "sql"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "bim_models", :force => true do |t|
    t.string   "name"
    t.string   "bim_path"
    t.float    "bim_site"
    t.text     "ifc_classes"
    t.integer  "sim_model_id"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  add_index "bim_models", ["sim_model_id"], :name => "index_bim_models_on_sim_model_id"

  create_table "building_elements", :force => true do |t|
    t.string   "globalid"
    t.string   "name"
    t.string   "ifc_class"
    t.string   "objectType"
    t.string   "ifc_line_id"
    t.string   "tag"
    t.string   "description"
    t.string   "level"
    t.string   "work_section"
    t.string   "material"
    t.string   "revit_id"
    t.float    "volume"
    t.float    "length"
    t.float    "depth"
    t.float    "width"
    t.integer  "primary_model_id"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
  end

  add_index "building_elements", ["primary_model_id"], :name => "index_building_elements_on_primary_model_id"

  create_table "construction_sites", :force => true do |t|
    t.integer "sim_model_id"
    t.integer "task_list_id"
  end

  add_index "construction_sites", ["sim_model_id"], :name => "index_construction_sites_on_sim_model_id"
  add_index "construction_sites", ["task_list_id"], :name => "index_construction_sites_on_task_list_id"

  create_table "draw_panels", :force => true do |t|
    t.string   "name"
    t.string   "sim_model_id"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "events", :force => true do |t|
    t.string   "name"
    t.datetime "start_at"
    t.datetime "end_at"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "gannt_charts", :force => true do |t|
    t.string   "name"
    t.integer  "sim_model_id"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  add_index "gannt_charts", ["sim_model_id"], :name => "index_gannt_charts_on_sim_model_id"

  create_table "ifc_objects", :force => true do |t|
    t.string "BIM_MODEL",                                      :limit => nil
    t.string "Projekt",                                        :limit => nil
    t.string "Klasse",                                         :limit => nil
    t.string "Kategorie",                                      :limit => nil
    t.string "Typ",                                            :limit => nil
    t.string "Bezeichnung",                                    :limit => nil
    t.string "Kommentar",                                      :limit => nil
    t.string "Bauabschnitt",                                   :limit => nil
    t.string "MU_class",                                       :limit => nil
    t.string "line_id",                                        :limit => nil
    t.string "globalId",                                       :limit => nil
    t.string "Code",                                           :limit => nil
    t.string "cpiFitMatchKey",                                 :limit => nil
    t.string "cpiID",                                          :limit => nil
    t.string "placement",                                      :limit => nil
    t.string "representation",                                 :limit => nil
    t.string "tag",                                            :limit => nil
    t.string "description",                                    :limit => nil
    t.string "objectType",                                     :limit => nil
    t.string "_4D_Code",                                       :limit => nil
    t.string "_4D_Element",                                    :limit => nil
    t.string "_4D_Level",                                      :limit => nil
    t.string "_4D_Material",                                   :limit => nil
    t.string "_4D_Site_Section",                               :limit => nil
    t.string "cpiComponentType",                               :limit => nil
    t.string "cpiSpaceBounding",                               :limit => nil
    t.string "Ebene",                                          :limit => nil
    t.string "Farbfuellung_fuer_groben_Massstab",              :limit => nil
    t.string "Flaeche",                                        :limit => nil
    t.string "FloorsLayer_00_MaterialID",                      :limit => nil
    t.string "FloorsLayer_00_MaterialName",                    :limit => nil
    t.string "FloorsLayer_00_Thickness",                       :limit => nil
    t.string "Fuellmuster_fuer_groben_Massstab",               :limit => nil
    t.string "Funktion",                                       :limit => nil
    t.string "Fuer_Koerper",                                   :limit => nil
    t.string "Hoehenversatz_von_Ebene",                        :limit => nil
    t.string "ID_DatenBearbeitungsbereich",                    :limit => nil
    t.string "MaterialName",                                   :limit => nil
    t.string "Modell",                                         :limit => nil
    t.string "Neigung",                                        :limit => nil
    t.string "NrLayers",                                       :limit => nil
    t.string "Raumbegrenzung",                                 :limit => nil
    t.string "RevitCategoryBuiltIn",                           :limit => nil
    t.string "RevitCategoryName",                              :limit => nil
    t.string "RevitID",                                        :limit => nil
    t.string "RevitLevelID",                                   :limit => nil
    t.string "RevitTypeName",                                  :limit => nil
    t.string "RevitFamilyName",                                :limit => nil
    t.string "RevitObjectType",                                :limit => nil
    t.string "Standarddicke",                                  :limit => nil
    t.string "Staerke",                                        :limit => nil
    t.string "Tragwerk",                                       :limit => nil
    t.string "Tragwerksverwendung",                            :limit => nil
    t.string "Typenmarkierung",                                :limit => nil
    t.string "Umfang",                                         :limit => nil
    t.string "Volumen",                                        :limit => nil
    t.string "Ansicht_unten",                                  :limit => nil
    t.string "Breite",                                         :limit => nil
    t.string "iTwo_Kategorie",                                 :limit => nil
    t.string "_Laenge",                                        :limit => nil
    t.string "StructuralFoundationLayer_00_MaterialID",        :limit => nil
    t.string "StructuralFoundationLayer_00_MaterialName",      :limit => nil
    t.string "StructuralFoundationLayer_00_Thickness",         :limit => nil
    t.string "Abhaengigkeit_oben",                             :limit => nil
    t.string "Abhaengigkeit_unten",                            :limit => nil
    t.string "Abschluss_an_Oeffnungen",                        :limit => nil
    t.string "Abschluss_an_Waenden",                           :limit => nil
    t.string "Basislinie",                                     :limit => nil
    t.string "Filter_Tragend_Nichttragend",                    :limit => nil
    t.string "Nicht_verknuepfte_Hoehe",                        :limit => nil
    t.string "Oberkante_ist_fixiert_",                         :limit => nil
    t.string "Typenkommentare",                                :limit => nil
    t.string "Unterkante_ist_fixiert",                         :limit => nil
    t.string "Verlaengerungsabstand_oben",                     :limit => nil
    t.string "Verlaengerungsabstand_unten",                    :limit => nil
    t.string "Versatz_oben",                                   :limit => nil
    t.string "Versatz_unten",                                  :limit => nil
    t.string "cpiComponentDirection",                          :limit => nil
    t.string "WallLayer_00_MaterialID",                        :limit => nil
    t.string "WallLayer_00_MaterialName",                      :limit => nil
    t.string "WallLayer_00_Thickness",                         :limit => nil
    t.string "Analysieren_als",                                :limit => nil
    t.string "Basisebene",                                     :limit => nil
    t.string "Bewehrungsueberdeckung_Andere_Flaechen",         :limit => nil
    t.string "Bewehrungsueberdeckung_Obere_Flaeche",           :limit => nil
    t.string "Bewehrungsueberdeckung_Untere_Flaeche",          :limit => nil
    t.string "cpiDisableCutOff",                               :limit => nil
    t.string "cpiDisableOpening",                              :limit => nil
    t.string "cpiLocation",                                    :limit => nil
    t.string "cpiLocation_Rotation",                           :limit => nil
    t.string "Exzentrische_Verbindungen",                      :limit => nil
    t.string "Freigabe_oben",                                  :limit => nil
    t.string "Freigabe_unten",                                 :limit => nil
    t.string "Fx_oben",                                        :limit => nil
    t.string "Fx_unten",                                       :limit => nil
    t.string "Fy_oben",                                        :limit => nil
    t.string "Fy_unten",                                       :limit => nil
    t.string "Fz_oben",                                        :limit => nil
    t.string "Fz_unten",                                       :limit => nil
    t.string "Mx_oben",                                        :limit => nil
    t.string "Mx_unten",                                       :limit => nil
    t.string "My_oben",                                        :limit => nil
    t.string "My_unten",                                       :limit => nil
    t.string "Mz_oben",                                        :limit => nil
    t.string "Mz_unten",                                       :limit => nil
    t.string "Oberste_Ebene",                                  :limit => nil
    t.string "Stuetzenmaterial",                               :limit => nil
    t.string "Stuetzenpositionsmarkierung",                    :limit => nil
    t.string "Stuetzenstil",                                   :limit => nil
    t.string "Tiefe",                                          :limit => nil
    t.string "Untere_vertikale_Position_der_Systemlinie",      :limit => nil
    t.string "Verschieben_mit_Raster",                         :limit => nil
    t.string "Aussparung_Breite",                              :limit => nil
    t.string "Aussparung_Hoehe",                               :limit => nil
    t.string "Aussparung_Laenge",                              :limit => nil
    t.string "Aussparung_Tiefe",                               :limit => nil
    t.string "Basisbauteil",                                   :limit => nil
    t.string "Bew_Schlaff_kg_mXB3",                            :limit => nil
    t.string "cpiWarning_ComponentType",                       :limit => nil
    t.string "ID_DatenGeaendert_von",                          :limit => nil
    t.string "Nebentraeger_Auflagertiefe",                     :limit => nil
    t.string "Nebentraeger_Breite_oben",                       :limit => nil
    t.string "Nebentraeger_Breite_unten",                      :limit => nil
    t.string "Nebentraeger_Deckendicke",                       :limit => nil
    t.string "Nebentraeger_Hoehe",                             :limit => nil
    t.string "Nebentraeger_Lichte_Laenge",                     :limit => nil
    t.string "Nebentraeger_Material",                          :limit => nil
    t.string "Versatz",                                        :limit => nil
    t.string "Verschieben_mit_umliegenden_Elementen",          :limit => nil
    t.string "A",                                              :limit => nil
    t.string "Arbeitsebene",                                   :limit => nil
    t.string "Ausrichtung",                                    :limit => nil
    t.string "Ausrichtung_in_z_Richtung",                      :limit => nil
    t.string "b",                                              :limit => nil
    t.string "Endebenenversatz",                               :limit => nil
    t.string "Enderweiterung",                                 :limit => nil
    t.string "Enderweiterung_berechnen",                       :limit => nil
    t.string "Freigabe_Anfang",                                :limit => nil
    t.string "Freigabe_Ende",                                  :limit => nil
    t.string "Fx_Anfang",                                      :limit => nil
    t.string "Fx_Ende",                                        :limit => nil
    t.string "Fy_Anfang",                                      :limit => nil
    t.string "Fy_Ende",                                        :limit => nil
    t.string "Fz_Anfang",                                      :limit => nil
    t.string "Fz_Ende",                                        :limit => nil
    t.string "g",                                              :limit => nil
    t.string "h",                                              :limit => nil
    t.string "k",                                              :limit => nil
    t.string "Mx_Anfang",                                      :limit => nil
    t.string "Mx_Ende",                                        :limit => nil
    t.string "My_Anfang",                                      :limit => nil
    t.string "My_Ende",                                        :limit => nil
    t.string "Mz_Anfang",                                      :limit => nil
    t.string "Mz_Ende",                                        :limit => nil
    t.string "r1",                                             :limit => nil
    t.string "Referenzebene",                                  :limit => nil
    t.string "RevitHost",                                      :limit => nil
    t.string "s",                                              :limit => nil
    t.string "Schnittlaenge",                                  :limit => nil
    t.string "Seitliche_Ausrichtung",                          :limit => nil
    t.string "t",                                              :limit => nil
    t.string "W",                                              :limit => nil
    t.string "bf",                                             :limit => nil
    t.string "d",                                              :limit => nil
    t.string "kr",                                             :limit => nil
    t.string "tf",                                             :limit => nil
    t.string "tw",                                             :limit => nil
    t.string "Durchmesser",                                    :limit => nil
    t.string "Stuetze_freihstehend",                           :limit => nil
    t.string "Stahlverbundtraeger_Auflagertiefe",              :limit => nil
    t.string "Stahlverbundtraeger_Breite",                     :limit => nil
    t.string "Stahlverbundtraeger_Deckendicke",                :limit => nil
    t.string "Stahlverbundtraeger_Hoehe",                      :limit => nil
    t.string "Stahlverbundtraeger_Lichte_Laenge",              :limit => nil
    t.string "Stahlverbundtraeger_Material",                   :limit => nil
    t.string "Ueberstandprofil_anwenden",                      :limit => nil
    t.string "Berechnungsmodell_aktivieren",                   :limit => nil
    t.string "Bewehrungsueberdeckung_Aussenflaeche",           :limit => nil
    t.string "Bewehrungsueberdeckung_Innenflaeche",            :limit => nil
    t.string "Ab_Beschriftung",                                :limit => nil
    t.string "Ab_Text",                                        :limit => nil
    t.string "Auf_Beschriftung",                               :limit => nil
    t.string "Auf_Text",                                       :limit => nil
    t.string "Form",                                           :limit => nil
    t.string "Maximale_Neigungslaenge",                        :limit => nil
    t.string "Maximale_Rampenneigung_1_x",                     :limit => nil
    t.string "Pfeile_nach_oben_in_allen_Ansichten_anzeigen",   :limit => nil
    t.string "Phase_erstellt",                                 :limit => nil
    t.string "Rampenmaterial",                                 :limit => nil
    t.string "Schriftart",                                     :limit => nil
    t.string "Textgroesse",                                    :limit => nil
    t.string "Beschreibung",                                   :limit => nil
    t.string "_4D_Element_No_",                                :limit => nil
    t.string "_4D_Element_Typ_No_",                            :limit => nil
    t.string "Unterzug_Breite",                                :limit => nil
    t.string "Unterzug_Deckendicke",                           :limit => nil
    t.string "Unterzug_Hoehe",                                 :limit => nil
    t.string "Unterzug_Konsolband_Breite",                     :limit => nil
    t.string "Unterzug_Konsolband_Hoehe",                      :limit => nil
    t.string "Unterzug_Laenge",                                :limit => nil
    t.string "Unterzug_Material",                              :limit => nil
    t.string "Haupttraeger_Breite",                            :limit => nil
    t.string "Haupttraeger_Deckendicke",                       :limit => nil
    t.string "Haupttraeger_Hoehe",                             :limit => nil
    t.string "Haupttraeger_Laenge",                            :limit => nil
    t.string "Endverbindung_Typ",                              :limit => nil
    t.string "_4D_X_Grid_Line",                                :limit => nil
    t.string "_4D_Y_Grid_Line",                                :limit => nil
    t.string "Kosten",                                         :limit => nil
    t.string "Anzahl_mittlerer_Wangen",                        :limit => nil
    t.string "Auftrittshoehe",                                 :limit => nil
    t.string "Bewehrungsueberdeckung",                         :limit => nil
    t.string "Bruchsymbol_im_Plan",                            :limit => nil
    t.string "Gewuenschte_Anzahl_an_Steigungen",               :limit => nil
    t.string "Linke_Wange",                                    :limit => nil
    t.string "Massivtreppe",                                   :limit => nil
    t.string "Material_Auftritt",                              :limit => nil
    t.string "Material_Massivtreppe",                          :limit => nil
    t.string "Material_Setzstufe",                             :limit => nil
    t.string "cpiPointCount",                                  :limit => nil
    t.string "Material_Wange",                                 :limit => nil
    t.string "Maximale_Stufenhoehe",                           :limit => nil
    t.string "Minimale_Auftrittsbreite",                       :limit => nil
    t.string "Mit_Setzstufe_beginnen",                         :limit => nil
    t.string "Mit_Setzstufe_enden",                            :limit => nil
    t.string "Pfeil_abwaerts",                                 :limit => nil
    t.string "Pfeil_aufwaerts",                                :limit => nil
    t.string "Rechte_Wange",                                   :limit => nil
    t.string "Setzstufendicke",                                :limit => nil
    t.string "Staerke_Laufplatte",                             :limit => nil
    t.string "Staerke_Podestplatte",                           :limit => nil
    t.string "Staerke_Wendelstufe",                            :limit => nil
    t.string "Steigungstyp",                                   :limit => nil
    t.string "Tatsaechliche_Anzahl_an_Steigungen",             :limit => nil
    t.string "Tatsaechliche_Auftrittsbreite",                  :limit => nil
    t.string "Tatsaechliche_Stufenhoehe",                      :limit => nil
    t.string "Trittueberstandslaenge",                         :limit => nil
    t.string "XDCberstandprofil_anwenden",                     :limit => nil
    t.string "Unterhalb_der_Basis_erweitern",                  :limit => nil
    t.string "Unterseite_der_Wendelstufe",                     :limit => nil
    t.string "Verbindung_Setzstufe_zu_Trittflaeche",           :limit => nil
    t.string "Versatz_Wange_bei_aufgesattelten_Stufen",        :limit => nil
    t.string "Wange_oben_abschneiden",                         :limit => nil
    t.string "Wangenhoehe",                                    :limit => nil
    t.string "Wangenstaerke",                                  :limit => nil
    t.string "Nebentraeger_Breite",                            :limit => nil
    t.string "Basis_mit_Rasterlinien_verschieben",             :limit => nil
    t.string "Basisschnitt_Stil",                              :limit => nil
    t.string "Basisverlaengerung",                             :limit => nil
    t.string "Obere_Verlaengerung",                            :limit => nil
    t.string "Oberer_Schnitt_Stil",                            :limit => nil
    t.string "Oberes_Ende_mit_Rasterlinien_verschieben",       :limit => nil
    t.string "Balkentyp",                                      :limit => nil
    t.string "Betonklasse",                                    :limit => nil
    t.string "Bew_Gittertraeger_kg_mXB2",                      :limit => nil
    t.string "Bew_Schlaff_kg_mXB2",                            :limit => nil
    t.string "Deckendicke",                                    :limit => nil
    t.string "Material",                                       :limit => nil
    t.string "Unterzug_Hoehe_Versatz",                         :limit => nil
    t.string "obj_Version",                                    :limit => nil
    t.string "OD",                                             :limit => nil
    t.string "Anfangsverbindung_Typ",                          :limit => nil
    t.string "Anfangversatz_berechnen",                        :limit => nil
    t.string "Endversatz_berechnen",                           :limit => nil
    t.string "Hoehe",                                          :limit => nil
    t.string "Kanten_durchgezogen",                            :limit => nil
    t.string "Maximaler_negativer_Versatz",                    :limit => nil
    t.string "Traeger",                                        :limit => nil
    t.string "Versatz_Anfang",                                 :limit => nil
    t.string "Versatz_Ende",                                   :limit => nil
    t.string "Winkel",                                         :limit => nil
    t.string "Fundament",                                      :limit => nil
    t.string "Untertzug_Laenge_Einschnitt",                    :limit => nil
    t.string "Unterzug_Beginn_Einschnitt",                     :limit => nil
    t.string "Unterzug_Tiefe_Einschnitt",                      :limit => nil
    t.string "Verbindungsausrichtung_unten",                   :limit => nil
    t.string "Versatz_von_Verbindung_unten",                   :limit => nil
    t.string "Fundamentstaerke",                               :limit => nil
    t.string "Fundamentueberstand_aussen",                     :limit => nil
    t.string "Fundamentueberstand_innen",                      :limit => nil
    t.string "Keine_Unterbrechung_bei_eingefuegten_Elementen", :limit => nil
    t.string "Standardlaenge_der_Enderweiterung",              :limit => nil
    t.string "Bedingung_fuer_gekruemmte_Kante",                :limit => nil
    t.string "Unterzug_Hoehe_Versatz_Achse_H2",                :limit => nil
    t.string "Unterzug_Hoehe_Versatz_Achse_J",                 :limit => nil
    t.string "Unterzug_Laenge_Einschnitt",                     :limit => nil
    t.string "Traeger_Breite",                                 :limit => nil
    t.string "Traeger_Deckendicke",                            :limit => nil
    t.string "Traeger_Hoehe",                                  :limit => nil
    t.string "Traeger_Laenge",                                 :limit => nil
    t.string "Traeger_Material",                               :limit => nil
    t.string "Composite",                                      :limit => nil
    t.string "Nettovolumen",                                   :limit => nil
    t.string "Nettoflaeche",                                   :limit => nil
    t.string "Ausladung1",                                     :limit => nil
    t.string "Nettogrundflaeche",                              :limit => nil
    t.string "Aufstellflaeche",                                :limit => nil
    t.string "maxTraglast_kg_",                                :limit => nil
    t.string "minTraglast_kg_",                                :limit => nil
    t.string "RadiusMaxTraglast",                              :limit => nil
    t.string "Koordinaten",                                    :limit => nil
    t.string "Einbahnstrasse",                                 :limit => nil
    t.string "Rettungsweg",                                    :limit => nil
    t.string "Dicke",                                          :limit => nil
    t.string "Anzahl_Fahrstreifen",                            :limit => nil
    t.string "Version",                                        :limit => nil
    t.string "UNIT_OF_MEASURE",                                :limit => nil
  end

  create_table "link_models", :force => true do |t|
    t.string   "linkmodeltype"
    t.string   "linkmodelid"
    t.string   "file"
    t.integer  "multi_model_container_id"
    t.datetime "created_at",               :null => false
    t.datetime "updated_at",               :null => false
  end

  add_index "link_models", ["multi_model_container_id"], :name => "index_link_models_on_multi_model_container_id"

  create_table "linking_tables", :force => true do |t|
    t.integer  "link_model_id"
    t.integer  "primary_model_id"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
  end

  create_table "links", :force => true do |t|
    t.string  "m1"
    t.string  "m2"
    t.string  "m3"
    t.string  "m4"
    t.string  "m5"
    t.string  "m6"
    t.string  "m7"
    t.string  "m8"
    t.string  "m9"
    t.string  "m10"
    t.string  "m1_namespace",  :limit => 40
    t.string  "m2_namespace",  :limit => 40
    t.string  "m3_namespace",  :limit => 40
    t.string  "m4_namespace",  :limit => 40
    t.string  "m5_namespace",  :limit => 40
    t.string  "m6_namespace",  :limit => 40
    t.string  "m7_namespace",  :limit => 40
    t.string  "m8_namespace",  :limit => 40
    t.string  "m9_namespace",  :limit => 40
    t.string  "m10_namespace", :limit => 40
    t.integer "m1_c"
    t.integer "m2_c"
    t.integer "m3_c"
    t.integer "m4_c"
    t.integer "m5_c"
    t.integer "m6_c"
    t.integer "m7_c"
    t.integer "m8_c"
    t.integer "m9_c"
    t.integer "m10_c"
    t.integer "link_model_id"
  end

  add_index "links", ["link_model_id"], :name => "index_links_on_link_model_id"

  create_table "lv_items", :force => true do |t|
    t.string   "item_id"
    t.string   "name"
    t.string   "rNOpaer",                  :limit => 100
    t.float    "qty"
    t.float    "predQty"
    t.string   "qu"
    t.float    "up"
    t.float    "it"
    t.text     "description_CompleteText"
    t.string   "description_OutlineText"
    t.float    "priority"
    t.string   "building_element_class"
    t.string   "default_calander"
    t.string   "duration_formula"
    t.text     "resources"
    t.text     "onStartScript"
    t.text     "onEndScript"
    t.text     "drawResourcesScript"
    t.string   "color"
    t.boolean  "export3D"
    t.text     "quantity_formula"
    t.datetime "created_at",                              :null => false
    t.datetime "updated_at",                              :null => false
  end

  create_table "mm_contents", :force => true do |t|
    t.string   "format"
    t.string   "contentid"
    t.string   "formatVersion"
    t.integer  "primary_model_id"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
  end

  add_index "mm_contents", ["primary_model_id"], :name => "index_mm_contents_on_primary_model_id"

  create_table "mm_files", :force => true do |t|
    t.string   "namespace"
    t.string   "file"
    t.integer  "mm_content_id"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  add_index "mm_files", ["mm_content_id"], :name => "index_mm_files_on_mm_content_id"

  create_table "multi_model_containers", :force => true do |t|
    t.string   "name"
    t.string   "mmc_file_path"
    t.string   "mmc_folder_path"
    t.datetime "meta_orgin_created"
    t.string   "meta_orgin_creatorId"
    t.string   "meta_orgin_application"
    t.string   "meta_orgin_appVersion"
    t.string   "guid"
    t.string   "formatVersion"
    t.datetime "created_at",             :null => false
    t.datetime "updated_at",             :null => false
  end

  create_table "primary_models", :force => true do |t|
    t.string   "modelType"
    t.string   "modelid"
    t.string   "contextId"
    t.integer  "multi_model_container_id"
    t.datetime "created_at",               :null => false
    t.datetime "updated_at",               :null => false
  end

  add_index "primary_models", ["multi_model_container_id"], :name => "index_primary_models_on_multi_model_container_id"

  create_table "process_pools", :force => true do |t|
    t.integer  "sim_model_id"
    t.text     "orgchart"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "project_monitors", :force => true do |t|
    t.string   "name"
    t.string   "sim_model_id"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "qto_items", :force => true do |t|
    t.string  "oz"
    t.string  "ort"
    t.string  "adresse"
    t.string  "formelnummer"
    t.float   "result"
    t.string  "rechenansatz"
    t.integer "building_element_id"
    t.integer "primary_model_id"
  end

  create_table "resource_pools", :force => true do |t|
    t.string   "name"
    t.integer  "sim_model_id"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "rpms", :force => true do |t|
    t.string   "name"
    t.string   "description"
    t.string   "tree_path"
    t.string   "bpmn_path"
    t.string   "img_path"
    t.integer  "user_id"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
    t.integer  "sim_model_id"
  end

# Could not dump table "sim_events" because of following StandardError
#   Unknown type 'real' for column 'changed_at'

  create_table "sim_models", :force => true do |t|
    t.string   "name"
    t.integer  "user_id"
    t.text     "description"
    t.string   "model_tree_path"
    t.string   "results_url"
    t.datetime "created_at",               :null => false
    t.datetime "updated_at",               :null => false
    t.integer  "multi_model_container_id"
    t.string   "bim_url"
  end

  create_table "sim_resources", :force => true do |t|
    t.string   "name"
    t.string   "res_class"
    t.float    "res_num"
    t.float    "delivery_time"
    t.datetime "delivery_date"
    t.integer  "resource_pool_id"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
  end

  create_table "sim_results", :force => true do |t|
    t.string   "name"
    t.string   "result_type"
    t.string   "result_url"
    t.text     "description"
    t.integer  "sim_szenario_id"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

  add_index "sim_results", ["sim_szenario_id"], :name => "index_sim_results_on_sim_szenario_id"

  create_table "sim_szenarios", :force => true do |t|
    t.string   "name"
    t.text     "description"
    t.integer  "sim_model_id"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "standard_colors", :force => true do |t|
    t.integer  "R"
    t.integer  "G"
    t.integer  "B"
    t.float    "A"
    t.integer  "RGB"
    t.string   "name"
    t.string   "name_de"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "standard_resources", :force => true do |t|
    t.string   "name"
    t.string   "description"
    t.integer  "parent_id"
    t.string   "category"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "task_attributes", :force => true do |t|
    t.string   "AttName"
    t.string   "AttValue"
    t.string   "AttType"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.integer  "task_id"
  end

  create_table "task_lists", :force => true do |t|
    t.string   "name"
    t.integer  "sim_model_id"
    t.string   "source_path"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "tasks", :force => true do |t|
    t.float    "start_time"
    t.datetime "start_date"
    t.integer  "task_numOfInstances"
    t.string   "name"
    t.string   "processTemplate"
    t.string   "ProcessPool"
    t.string   "duration"
    t.string   "duration_formula"
    t.string   "predecessor"
    t.float    "predecessor_puffer"
    t.string   "globalId"
    t.integer  "buildingElement_id"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
    t.string   "permalink"
    t.integer  "task_list_id"
    t.datetime "end_date"
  end

  create_table "tower_cranes", :force => true do |t|
    t.string   "name"
    t.string   "craneModel"
    t.float    "rotation_speed"
    t.float    "trolly_speed"
    t.float    "z_speed"
    t.integer  "draw_layer"
    t.string   "resource_pool"
    t.string   "obj_class"
    t.string   "sim_obj_path"
    t.float    "xPos"
    t.float    "yPos"
    t.float    "raduis"
    t.float    "height"
    t.float    "baseWidth"
    t.time     "start_time"
    t.time     "end_time"
    t.datetime "start_date"
    t.datetime "end_date"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
    t.integer  "resource_pool_id"
  end

  create_table "users", :force => true do |t|
    t.string   "name"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
    t.string   "email",                  :default => "", :null => false
    t.string   "encrypted_password",     :default => "", :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
  end

  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true

end
