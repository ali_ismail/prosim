require 'test_helper'

class AttributsControllerTest < ActionController::TestCase
  setup do
    @attribut = attributs(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:attributs)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create attribut" do
    assert_difference('Attribut.count') do
      post :create, attribut: { attType: @attribut.attType, dflt_value: @attribut.dflt_value, name: @attribut.name, notnull: @attribut.notnull }
    end

    assert_redirected_to attribut_path(assigns(:attribut))
  end

  test "should show attribut" do
    get :show, id: @attribut
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @attribut
    assert_response :success
  end

  test "should update attribut" do
    put :update, id: @attribut, attribut: { attType: @attribut.attType, dflt_value: @attribut.dflt_value, name: @attribut.name, notnull: @attribut.notnull }
    assert_redirected_to attribut_path(assigns(:attribut))
  end

  test "should destroy attribut" do
    assert_difference('Attribut.count', -1) do
      delete :destroy, id: @attribut
    end

    assert_redirected_to attributs_path
  end
end
