require 'test_helper'

class BimFiltersControllerTest < ActionController::TestCase
  setup do
    @bim_filter = bim_filters(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:bim_filters)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create bim_filter" do
    assert_difference('BimFilter.count') do
      post :create, bim_filter: { name: @bim_filter.name, sql: @bim_filter.sql }
    end

    assert_redirected_to bim_filter_path(assigns(:bim_filter))
  end

  test "should show bim_filter" do
    get :show, id: @bim_filter
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @bim_filter
    assert_response :success
  end

  test "should update bim_filter" do
    put :update, id: @bim_filter, bim_filter: { name: @bim_filter.name, sql: @bim_filter.sql }
    assert_redirected_to bim_filter_path(assigns(:bim_filter))
  end

  test "should destroy bim_filter" do
    assert_difference('BimFilter.count', -1) do
      delete :destroy, id: @bim_filter
    end

    assert_redirected_to bim_filters_path
  end
end
