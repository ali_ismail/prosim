require 'test_helper'

class BimModelsControllerTest < ActionController::TestCase
  setup do
    @bim_model = bim_models(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:bim_models)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create bim_model" do
    assert_difference('BimModel.count') do
      post :create, bim_model: { bim_path: @bim_model.bim_path, bim_site: @bim_model.bim_site, ifc_classes: @bim_model.ifc_classes, name: @bim_model.name }
    end

    assert_redirected_to bim_model_path(assigns(:bim_model))
  end

  test "should show bim_model" do
    get :show, id: @bim_model
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @bim_model
    assert_response :success
  end

  test "should update bim_model" do
    put :update, id: @bim_model, bim_model: { bim_path: @bim_model.bim_path, bim_site: @bim_model.bim_site, ifc_classes: @bim_model.ifc_classes, name: @bim_model.name }
    assert_redirected_to bim_model_path(assigns(:bim_model))
  end

  test "should destroy bim_model" do
    assert_difference('BimModel.count', -1) do
      delete :destroy, id: @bim_model
    end

    assert_redirected_to bim_models_path
  end
end
