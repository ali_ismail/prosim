require 'test_helper'

class BuildingElementsControllerTest < ActionController::TestCase
  setup do
    @building_element = building_elements(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:building_elements)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create building_element" do
    assert_difference('BuildingElement.count') do
      post :create, building_element: { depth: @building_element.depth, description: @building_element.description, globalid: @building_element.globalid, ifc_class: @building_element.ifc_class, ifc_line_id: @building_element.ifc_line_id, length: @building_element.length, level: @building_element.level, material: @building_element.material, name: @building_element.name, objectType: @building_element.objectType, revit_id: @building_element.revit_id, tag: @building_element.tag, volume: @building_element.volume, width: @building_element.width, work_section: @building_element.work_section }
    end

    assert_redirected_to building_element_path(assigns(:building_element))
  end

  test "should show building_element" do
    get :show, id: @building_element
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @building_element
    assert_response :success
  end

  test "should update building_element" do
    put :update, id: @building_element, building_element: { depth: @building_element.depth, description: @building_element.description, globalid: @building_element.globalid, ifc_class: @building_element.ifc_class, ifc_line_id: @building_element.ifc_line_id, length: @building_element.length, level: @building_element.level, material: @building_element.material, name: @building_element.name, objectType: @building_element.objectType, revit_id: @building_element.revit_id, tag: @building_element.tag, volume: @building_element.volume, width: @building_element.width, work_section: @building_element.work_section }
    assert_redirected_to building_element_path(assigns(:building_element))
  end

  test "should destroy building_element" do
    assert_difference('BuildingElement.count', -1) do
      delete :destroy, id: @building_element
    end

    assert_redirected_to building_elements_path
  end
end
