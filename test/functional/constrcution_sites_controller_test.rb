require 'test_helper'

class ConstrcutionSitesControllerTest < ActionController::TestCase
  setup do
    @constrcution_site = constrcution_sites(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:constrcution_sites)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create constrcution_site" do
    assert_difference('ConstrcutionSite.count') do
      post :create, constrcution_site: {  }
    end

    assert_redirected_to constrcution_site_path(assigns(:constrcution_site))
  end

  test "should show constrcution_site" do
    get :show, id: @constrcution_site
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @constrcution_site
    assert_response :success
  end

  test "should update constrcution_site" do
    put :update, id: @constrcution_site, constrcution_site: {  }
    assert_redirected_to constrcution_site_path(assigns(:constrcution_site))
  end

  test "should destroy constrcution_site" do
    assert_difference('ConstrcutionSite.count', -1) do
      delete :destroy, id: @constrcution_site
    end

    assert_redirected_to constrcution_sites_path
  end
end
