require 'test_helper'

class DrawPanelsControllerTest < ActionController::TestCase
  setup do
    @draw_panel = draw_panels(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:draw_panels)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create draw_panel" do
    assert_difference('DrawPanel.count') do
      post :create, draw_panel: { name: @draw_panel.name, sim_model_id: @draw_panel.sim_model_id }
    end

    assert_redirected_to draw_panel_path(assigns(:draw_panel))
  end

  test "should show draw_panel" do
    get :show, id: @draw_panel
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @draw_panel
    assert_response :success
  end

  test "should update draw_panel" do
    put :update, id: @draw_panel, draw_panel: { name: @draw_panel.name, sim_model_id: @draw_panel.sim_model_id }
    assert_redirected_to draw_panel_path(assigns(:draw_panel))
  end

  test "should destroy draw_panel" do
    assert_difference('DrawPanel.count', -1) do
      delete :destroy, id: @draw_panel
    end

    assert_redirected_to draw_panels_path
  end
end
