require 'test_helper'

class GanntChartsControllerTest < ActionController::TestCase
  setup do
    @gannt_chart = gannt_charts(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:gannt_charts)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create gannt_chart" do
    assert_difference('GanntChart.count') do
      post :create, gannt_chart: { name: @gannt_chart.name }
    end

    assert_redirected_to gannt_chart_path(assigns(:gannt_chart))
  end

  test "should show gannt_chart" do
    get :show, id: @gannt_chart
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @gannt_chart
    assert_response :success
  end

  test "should update gannt_chart" do
    put :update, id: @gannt_chart, gannt_chart: { name: @gannt_chart.name }
    assert_redirected_to gannt_chart_path(assigns(:gannt_chart))
  end

  test "should destroy gannt_chart" do
    assert_difference('GanntChart.count', -1) do
      delete :destroy, id: @gannt_chart
    end

    assert_redirected_to gannt_charts_path
  end
end
