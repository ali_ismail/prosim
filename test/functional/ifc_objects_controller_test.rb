require 'test_helper'

class IfcObjectsControllerTest < ActionController::TestCase
  setup do
    @ifc_object = ifc_objects(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:ifc_objects)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create ifc_object" do
    assert_difference('IfcObject.count') do
      post :create, ifc_object: {  }
    end

    assert_redirected_to ifc_object_path(assigns(:ifc_object))
  end

  test "should show ifc_object" do
    get :show, id: @ifc_object
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @ifc_object
    assert_response :success
  end

  test "should update ifc_object" do
    put :update, id: @ifc_object, ifc_object: {  }
    assert_redirected_to ifc_object_path(assigns(:ifc_object))
  end

  test "should destroy ifc_object" do
    assert_difference('IfcObject.count', -1) do
      delete :destroy, id: @ifc_object
    end

    assert_redirected_to ifc_objects_path
  end
end
