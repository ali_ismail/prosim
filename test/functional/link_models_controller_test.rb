require 'test_helper'

class LinkModelsControllerTest < ActionController::TestCase
  setup do
    @link_model = link_models(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:link_models)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create link_model" do
    assert_difference('LinkModel.count') do
      post :create, link_model: { file: @link_model.file, linkmodelid: @link_model.linkmodelid, linkmodeltype: @link_model.linkmodeltype }
    end

    assert_redirected_to link_model_path(assigns(:link_model))
  end

  test "should show link_model" do
    get :show, id: @link_model
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @link_model
    assert_response :success
  end

  test "should update link_model" do
    put :update, id: @link_model, link_model: { file: @link_model.file, linkmodelid: @link_model.linkmodelid, linkmodeltype: @link_model.linkmodeltype }
    assert_redirected_to link_model_path(assigns(:link_model))
  end

  test "should destroy link_model" do
    assert_difference('LinkModel.count', -1) do
      delete :destroy, id: @link_model
    end

    assert_redirected_to link_models_path
  end
end
