require 'test_helper'

class LvItemsControllerTest < ActionController::TestCase
  setup do
    @lv_item = lv_items(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:lv_items)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create lv_item" do
    assert_difference('LvItem.count') do
      post :create, lv_item: { building_element_class: @lv_item.building_element_class, color: @lv_item.color, default_calander: @lv_item.default_calander, description_CompleteText: @lv_item.description_CompleteText, description_OutlineText: @lv_item.description_OutlineText, drawResourcesScript: @lv_item.drawResourcesScript, duration_formula: @lv_item.duration_formula, export3D: @lv_item.export3D, it: @lv_item.it, item_id: @lv_item.item_id, name: @lv_item.name, onEndScript: @lv_item.onEndScript, onStartScript: @lv_item.onStartScript, predQty: @lv_item.predQty, priority: @lv_item.priority, qty: @lv_item.qty, qu: @lv_item.qu, quantity_formula: @lv_item.quantity_formula, rNOpaer: @lv_item.rNOpaer, resources: @lv_item.resources, up: @lv_item.up }
    end

    assert_redirected_to lv_item_path(assigns(:lv_item))
  end

  test "should show lv_item" do
    get :show, id: @lv_item
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @lv_item
    assert_response :success
  end

  test "should update lv_item" do
    put :update, id: @lv_item, lv_item: { building_element_class: @lv_item.building_element_class, color: @lv_item.color, default_calander: @lv_item.default_calander, description_CompleteText: @lv_item.description_CompleteText, description_OutlineText: @lv_item.description_OutlineText, drawResourcesScript: @lv_item.drawResourcesScript, duration_formula: @lv_item.duration_formula, export3D: @lv_item.export3D, it: @lv_item.it, item_id: @lv_item.item_id, name: @lv_item.name, onEndScript: @lv_item.onEndScript, onStartScript: @lv_item.onStartScript, predQty: @lv_item.predQty, priority: @lv_item.priority, qty: @lv_item.qty, qu: @lv_item.qu, quantity_formula: @lv_item.quantity_formula, rNOpaer: @lv_item.rNOpaer, resources: @lv_item.resources, up: @lv_item.up }
    assert_redirected_to lv_item_path(assigns(:lv_item))
  end

  test "should destroy lv_item" do
    assert_difference('LvItem.count', -1) do
      delete :destroy, id: @lv_item
    end

    assert_redirected_to lv_items_path
  end
end
