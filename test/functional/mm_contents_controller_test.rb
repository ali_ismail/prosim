require 'test_helper'

class MmContentsControllerTest < ActionController::TestCase
  setup do
    @mm_content = mm_contents(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:mm_contents)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create mm_content" do
    assert_difference('MmContent.count') do
      post :create, mm_content: { contentid: @mm_content.contentid, format: @mm_content.format, formatVersion: @mm_content.formatVersion }
    end

    assert_redirected_to mm_content_path(assigns(:mm_content))
  end

  test "should show mm_content" do
    get :show, id: @mm_content
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @mm_content
    assert_response :success
  end

  test "should update mm_content" do
    put :update, id: @mm_content, mm_content: { contentid: @mm_content.contentid, format: @mm_content.format, formatVersion: @mm_content.formatVersion }
    assert_redirected_to mm_content_path(assigns(:mm_content))
  end

  test "should destroy mm_content" do
    assert_difference('MmContent.count', -1) do
      delete :destroy, id: @mm_content
    end

    assert_redirected_to mm_contents_path
  end
end
