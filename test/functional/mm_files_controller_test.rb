require 'test_helper'

class MmFilesControllerTest < ActionController::TestCase
  setup do
    @mm_file = mm_files(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:mm_files)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create mm_file" do
    assert_difference('MmFile.count') do
      post :create, mm_file: { file: @mm_file.file, namespace: @mm_file.namespace }
    end

    assert_redirected_to mm_file_path(assigns(:mm_file))
  end

  test "should show mm_file" do
    get :show, id: @mm_file
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @mm_file
    assert_response :success
  end

  test "should update mm_file" do
    put :update, id: @mm_file, mm_file: { file: @mm_file.file, namespace: @mm_file.namespace }
    assert_redirected_to mm_file_path(assigns(:mm_file))
  end

  test "should destroy mm_file" do
    assert_difference('MmFile.count', -1) do
      delete :destroy, id: @mm_file
    end

    assert_redirected_to mm_files_path
  end
end
