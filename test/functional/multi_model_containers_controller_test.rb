require 'test_helper'

class MultiModelContainersControllerTest < ActionController::TestCase
  setup do
    @multi_model_container = multi_model_containers(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:multi_model_containers)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create multi_model_container" do
    assert_difference('MultiModelContainer.count') do
      post :create, multi_model_container: { formatVersion: @multi_model_container.formatVersion, guid: @multi_model_container.guid, meta_orgin_appVersion: @multi_model_container.meta_orgin_appVersion, meta_orgin_application: @multi_model_container.meta_orgin_application, meta_orgin_created: @multi_model_container.meta_orgin_created, meta_orgin_creatorId: @multi_model_container.meta_orgin_creatorId, mmc_file_path: @multi_model_container.mmc_file_path, mmc_folder_path: @multi_model_container.mmc_folder_path, name: @multi_model_container.name }
    end

    assert_redirected_to multi_model_container_path(assigns(:multi_model_container))
  end

  test "should show multi_model_container" do
    get :show, id: @multi_model_container
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @multi_model_container
    assert_response :success
  end

  test "should update multi_model_container" do
    put :update, id: @multi_model_container, multi_model_container: { formatVersion: @multi_model_container.formatVersion, guid: @multi_model_container.guid, meta_orgin_appVersion: @multi_model_container.meta_orgin_appVersion, meta_orgin_application: @multi_model_container.meta_orgin_application, meta_orgin_created: @multi_model_container.meta_orgin_created, meta_orgin_creatorId: @multi_model_container.meta_orgin_creatorId, mmc_file_path: @multi_model_container.mmc_file_path, mmc_folder_path: @multi_model_container.mmc_folder_path, name: @multi_model_container.name }
    assert_redirected_to multi_model_container_path(assigns(:multi_model_container))
  end

  test "should destroy multi_model_container" do
    assert_difference('MultiModelContainer.count', -1) do
      delete :destroy, id: @multi_model_container
    end

    assert_redirected_to multi_model_containers_path
  end
end
