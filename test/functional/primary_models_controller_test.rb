require 'test_helper'

class PrimaryModelsControllerTest < ActionController::TestCase
  setup do
    @primary_model = primary_models(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:primary_models)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create primary_model" do
    assert_difference('PrimaryModel.count') do
      post :create, primary_model: { contextId: @primary_model.contextId, modelType: @primary_model.modelType, modelid: @primary_model.modelid }
    end

    assert_redirected_to primary_model_path(assigns(:primary_model))
  end

  test "should show primary_model" do
    get :show, id: @primary_model
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @primary_model
    assert_response :success
  end

  test "should update primary_model" do
    put :update, id: @primary_model, primary_model: { contextId: @primary_model.contextId, modelType: @primary_model.modelType, modelid: @primary_model.modelid }
    assert_redirected_to primary_model_path(assigns(:primary_model))
  end

  test "should destroy primary_model" do
    assert_difference('PrimaryModel.count', -1) do
      delete :destroy, id: @primary_model
    end

    assert_redirected_to primary_models_path
  end
end
