require 'test_helper'

class ProcessPoolsControllerTest < ActionController::TestCase
  setup do
    @process_pool = process_pools(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:process_pools)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create process_pool" do
    assert_difference('ProcessPool.count') do
      post :create, process_pool: { orgchart: @process_pool.orgchart, sim_model_id: @process_pool.sim_model_id }
    end

    assert_redirected_to process_pool_path(assigns(:process_pool))
  end

  test "should show process_pool" do
    get :show, id: @process_pool
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @process_pool
    assert_response :success
  end

  test "should update process_pool" do
    put :update, id: @process_pool, process_pool: { orgchart: @process_pool.orgchart, sim_model_id: @process_pool.sim_model_id }
    assert_redirected_to process_pool_path(assigns(:process_pool))
  end

  test "should destroy process_pool" do
    assert_difference('ProcessPool.count', -1) do
      delete :destroy, id: @process_pool
    end

    assert_redirected_to process_pools_path
  end
end
