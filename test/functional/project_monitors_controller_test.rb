require 'test_helper'

class ProjectMonitorsControllerTest < ActionController::TestCase
  setup do
    @project_monitor = project_monitors(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:project_monitors)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create project_monitor" do
    assert_difference('ProjectMonitor.count') do
      post :create, project_monitor: { name: @project_monitor.name, sim_model_id: @project_monitor.sim_model_id }
    end

    assert_redirected_to project_monitor_path(assigns(:project_monitor))
  end

  test "should show project_monitor" do
    get :show, id: @project_monitor
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @project_monitor
    assert_response :success
  end

  test "should update project_monitor" do
    put :update, id: @project_monitor, project_monitor: { name: @project_monitor.name, sim_model_id: @project_monitor.sim_model_id }
    assert_redirected_to project_monitor_path(assigns(:project_monitor))
  end

  test "should destroy project_monitor" do
    assert_difference('ProjectMonitor.count', -1) do
      delete :destroy, id: @project_monitor
    end

    assert_redirected_to project_monitors_path
  end
end
