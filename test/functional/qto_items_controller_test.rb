require 'test_helper'

class QtoItemsControllerTest < ActionController::TestCase
  setup do
    @qto_item = qto_items(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:qto_items)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create qto_item" do
    assert_difference('QtoItem.count') do
      post :create, qto_item: { adresse: @qto_item.adresse, formelnummer: @qto_item.formelnummer, ort: @qto_item.ort, oz: @qto_item.oz, rechenansatz: @qto_item.rechenansatz, result: @qto_item.result }
    end

    assert_redirected_to qto_item_path(assigns(:qto_item))
  end

  test "should show qto_item" do
    get :show, id: @qto_item
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @qto_item
    assert_response :success
  end

  test "should update qto_item" do
    put :update, id: @qto_item, qto_item: { adresse: @qto_item.adresse, formelnummer: @qto_item.formelnummer, ort: @qto_item.ort, oz: @qto_item.oz, rechenansatz: @qto_item.rechenansatz, result: @qto_item.result }
    assert_redirected_to qto_item_path(assigns(:qto_item))
  end

  test "should destroy qto_item" do
    assert_difference('QtoItem.count', -1) do
      delete :destroy, id: @qto_item
    end

    assert_redirected_to qto_items_path
  end
end
