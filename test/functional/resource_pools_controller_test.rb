require 'test_helper'

class ResourcePoolsControllerTest < ActionController::TestCase
  setup do
    @resource_pool = resource_pools(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:resource_pools)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create resource_pool" do
    assert_difference('ResourcePool.count') do
      post :create, resource_pool: { name: @resource_pool.name, sim_model_id: @resource_pool.sim_model_id }
    end

    assert_redirected_to resource_pool_path(assigns(:resource_pool))
  end

  test "should show resource_pool" do
    get :show, id: @resource_pool
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @resource_pool
    assert_response :success
  end

  test "should update resource_pool" do
    put :update, id: @resource_pool, resource_pool: { name: @resource_pool.name, sim_model_id: @resource_pool.sim_model_id }
    assert_redirected_to resource_pool_path(assigns(:resource_pool))
  end

  test "should destroy resource_pool" do
    assert_difference('ResourcePool.count', -1) do
      delete :destroy, id: @resource_pool
    end

    assert_redirected_to resource_pools_path
  end
end
