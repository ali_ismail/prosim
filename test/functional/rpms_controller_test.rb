require 'test_helper'

class RpmsControllerTest < ActionController::TestCase
  setup do
    @rpm = rpms(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:rpms)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create rpm" do
    assert_difference('Rpm.count') do
      post :create, rpm: { bpmn_path: @rpm.bpmn_path, description: @rpm.description, img_path: @rpm.img_path, name: @rpm.name, tree_path: @rpm.tree_path, user_id: @rpm.user_id }
    end

    assert_redirected_to rpm_path(assigns(:rpm))
  end

  test "should show rpm" do
    get :show, id: @rpm
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @rpm
    assert_response :success
  end

  test "should update rpm" do
    put :update, id: @rpm, rpm: { bpmn_path: @rpm.bpmn_path, description: @rpm.description, img_path: @rpm.img_path, name: @rpm.name, tree_path: @rpm.tree_path, user_id: @rpm.user_id }
    assert_redirected_to rpm_path(assigns(:rpm))
  end

  test "should destroy rpm" do
    assert_difference('Rpm.count', -1) do
      delete :destroy, id: @rpm
    end

    assert_redirected_to rpms_path
  end
end
