require 'test_helper'

class SimEventsControllerTest < ActionController::TestCase
  setup do
    @sim_event = sim_events(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:sim_events)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create sim_event" do
    assert_difference('SimEvent.count') do
      post :create, sim_event: { changed_at: @sim_event.changed_at, obj_status: @sim_event.obj_status, object: @sim_event.object }
    end

    assert_redirected_to sim_event_path(assigns(:sim_event))
  end

  test "should show sim_event" do
    get :show, id: @sim_event
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @sim_event
    assert_response :success
  end

  test "should update sim_event" do
    put :update, id: @sim_event, sim_event: { changed_at: @sim_event.changed_at, obj_status: @sim_event.obj_status, object: @sim_event.object }
    assert_redirected_to sim_event_path(assigns(:sim_event))
  end

  test "should destroy sim_event" do
    assert_difference('SimEvent.count', -1) do
      delete :destroy, id: @sim_event
    end

    assert_redirected_to sim_events_path
  end
end
