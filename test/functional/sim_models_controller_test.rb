require 'test_helper'

class SimModelsControllerTest < ActionController::TestCase
  setup do
    @sim_model = sim_models(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:sim_models)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create sim_model" do
    assert_difference('SimModel.count') do
      post :create, sim_model: { description: @sim_model.description, model_tree_path: @sim_model.model_tree_path, name: @sim_model.name, results_url: @sim_model.results_url, user_id: @sim_model.user_id }
    end

    assert_redirected_to sim_model_path(assigns(:sim_model))
  end

  test "should show sim_model" do
    get :show, id: @sim_model
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @sim_model
    assert_response :success
  end

  test "should update sim_model" do
    put :update, id: @sim_model, sim_model: { description: @sim_model.description, model_tree_path: @sim_model.model_tree_path, name: @sim_model.name, results_url: @sim_model.results_url, user_id: @sim_model.user_id }
    assert_redirected_to sim_model_path(assigns(:sim_model))
  end

  test "should destroy sim_model" do
    assert_difference('SimModel.count', -1) do
      delete :destroy, id: @sim_model
    end

    assert_redirected_to sim_models_path
  end
end
