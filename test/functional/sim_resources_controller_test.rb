require 'test_helper'

class SimResourcesControllerTest < ActionController::TestCase
  setup do
    @sim_resource = sim_resources(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:sim_resources)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create sim_resource" do
    assert_difference('SimResource.count') do
      post :create, sim_resource: { delivery_date: @sim_resource.delivery_date, delivery_time: @sim_resource.delivery_time, name: @sim_resource.name, res_class: @sim_resource.res_class, res_num: @sim_resource.res_num, sim_model_id: @sim_resource.sim_model_id }
    end

    assert_redirected_to sim_resource_path(assigns(:sim_resource))
  end

  test "should show sim_resource" do
    get :show, id: @sim_resource
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @sim_resource
    assert_response :success
  end

  test "should update sim_resource" do
    put :update, id: @sim_resource, sim_resource: { delivery_date: @sim_resource.delivery_date, delivery_time: @sim_resource.delivery_time, name: @sim_resource.name, res_class: @sim_resource.res_class, res_num: @sim_resource.res_num, sim_model_id: @sim_resource.sim_model_id }
    assert_redirected_to sim_resource_path(assigns(:sim_resource))
  end

  test "should destroy sim_resource" do
    assert_difference('SimResource.count', -1) do
      delete :destroy, id: @sim_resource
    end

    assert_redirected_to sim_resources_path
  end
end
