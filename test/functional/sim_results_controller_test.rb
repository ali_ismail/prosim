require 'test_helper'

class SimResultsControllerTest < ActionController::TestCase
  setup do
    @sim_result = sim_results(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:sim_results)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create sim_result" do
    assert_difference('SimResult.count') do
      post :create, sim_result: { description: @sim_result.description, name: @sim_result.name, result_type: @sim_result.result_type, result_url: @sim_result.result_url }
    end

    assert_redirected_to sim_result_path(assigns(:sim_result))
  end

  test "should show sim_result" do
    get :show, id: @sim_result
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @sim_result
    assert_response :success
  end

  test "should update sim_result" do
    put :update, id: @sim_result, sim_result: { description: @sim_result.description, name: @sim_result.name, result_type: @sim_result.result_type, result_url: @sim_result.result_url }
    assert_redirected_to sim_result_path(assigns(:sim_result))
  end

  test "should destroy sim_result" do
    assert_difference('SimResult.count', -1) do
      delete :destroy, id: @sim_result
    end

    assert_redirected_to sim_results_path
  end
end
