require 'test_helper'

class SimSzenariosControllerTest < ActionController::TestCase
  setup do
    @sim_szenario = sim_szenarios(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:sim_szenarios)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create sim_szenario" do
    assert_difference('SimSzenario.count') do
      post :create, sim_szenario: { description: @sim_szenario.description, name: @sim_szenario.name }
    end

    assert_redirected_to sim_szenario_path(assigns(:sim_szenario))
  end

  test "should show sim_szenario" do
    get :show, id: @sim_szenario
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @sim_szenario
    assert_response :success
  end

  test "should update sim_szenario" do
    put :update, id: @sim_szenario, sim_szenario: { description: @sim_szenario.description, name: @sim_szenario.name }
    assert_redirected_to sim_szenario_path(assigns(:sim_szenario))
  end

  test "should destroy sim_szenario" do
    assert_difference('SimSzenario.count', -1) do
      delete :destroy, id: @sim_szenario
    end

    assert_redirected_to sim_szenarios_path
  end
end
