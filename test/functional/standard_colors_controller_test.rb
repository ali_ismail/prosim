require 'test_helper'

class StandardColorsControllerTest < ActionController::TestCase
  setup do
    @standard_color = standard_colors(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:standard_colors)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create standard_color" do
    assert_difference('StandardColor.count') do
      post :create, standard_color: { A: @standard_color.A, B: @standard_color.B, G: @standard_color.G, R: @standard_color.R, RGB: @standard_color.RGB, name: @standard_color.name, name_de: @standard_color.name_de }
    end

    assert_redirected_to standard_color_path(assigns(:standard_color))
  end

  test "should show standard_color" do
    get :show, id: @standard_color
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @standard_color
    assert_response :success
  end

  test "should update standard_color" do
    put :update, id: @standard_color, standard_color: { A: @standard_color.A, B: @standard_color.B, G: @standard_color.G, R: @standard_color.R, RGB: @standard_color.RGB, name: @standard_color.name, name_de: @standard_color.name_de }
    assert_redirected_to standard_color_path(assigns(:standard_color))
  end

  test "should destroy standard_color" do
    assert_difference('StandardColor.count', -1) do
      delete :destroy, id: @standard_color
    end

    assert_redirected_to standard_colors_path
  end
end
