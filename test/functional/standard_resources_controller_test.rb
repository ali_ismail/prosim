require 'test_helper'

class StandardResourcesControllerTest < ActionController::TestCase
  setup do
    @standard_resource = standard_resources(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:standard_resources)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create standard_resource" do
    assert_difference('StandardResource.count') do
      post :create, standard_resource: { category: @standard_resource.category, description: @standard_resource.description, name: @standard_resource.name, parent_id: @standard_resource.parent_id }
    end

    assert_redirected_to standard_resource_path(assigns(:standard_resource))
  end

  test "should show standard_resource" do
    get :show, id: @standard_resource
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @standard_resource
    assert_response :success
  end

  test "should update standard_resource" do
    put :update, id: @standard_resource, standard_resource: { category: @standard_resource.category, description: @standard_resource.description, name: @standard_resource.name, parent_id: @standard_resource.parent_id }
    assert_redirected_to standard_resource_path(assigns(:standard_resource))
  end

  test "should destroy standard_resource" do
    assert_difference('StandardResource.count', -1) do
      delete :destroy, id: @standard_resource
    end

    assert_redirected_to standard_resources_path
  end
end
