require 'test_helper'

class TaskAttributesControllerTest < ActionController::TestCase
  setup do
    @task_attribute = task_attributes(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:task_attributes)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create task_attribute" do
    assert_difference('TaskAttribute.count') do
      post :create, task_attribute: { AttName: @task_attribute.AttName, AttType: @task_attribute.AttType, AttValue: @task_attribute.AttValue }
    end

    assert_redirected_to task_attribute_path(assigns(:task_attribute))
  end

  test "should show task_attribute" do
    get :show, id: @task_attribute
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @task_attribute
    assert_response :success
  end

  test "should update task_attribute" do
    put :update, id: @task_attribute, task_attribute: { AttName: @task_attribute.AttName, AttType: @task_attribute.AttType, AttValue: @task_attribute.AttValue }
    assert_redirected_to task_attribute_path(assigns(:task_attribute))
  end

  test "should destroy task_attribute" do
    assert_difference('TaskAttribute.count', -1) do
      delete :destroy, id: @task_attribute
    end

    assert_redirected_to task_attributes_path
  end
end
