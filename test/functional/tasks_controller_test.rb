require 'test_helper'

class TasksControllerTest < ActionController::TestCase
  setup do
    @task = tasks(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:tasks)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create task" do
    assert_difference('Task.count') do
      post :create, task: { ProcessPool: @task.ProcessPool, buildingElement_id: @task.buildingElement_id, duration: @task.duration, duration_formula: @task.duration_formula, globalId: @task.globalId, name: @task.name, predecessor: @task.predecessor, predecessor_puffer: @task.predecessor_puffer, processTemplate: @task.processTemplate, start_date: @task.start_date, start_time: @task.start_time, task_numOfInstances: @task.task_numOfInstances }
    end

    assert_redirected_to task_path(assigns(:task))
  end

  test "should show task" do
    get :show, id: @task
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @task
    assert_response :success
  end

  test "should update task" do
    put :update, id: @task, task: { ProcessPool: @task.ProcessPool, buildingElement_id: @task.buildingElement_id, duration: @task.duration, duration_formula: @task.duration_formula, globalId: @task.globalId, name: @task.name, predecessor: @task.predecessor, predecessor_puffer: @task.predecessor_puffer, processTemplate: @task.processTemplate, start_date: @task.start_date, start_time: @task.start_time, task_numOfInstances: @task.task_numOfInstances }
    assert_redirected_to task_path(assigns(:task))
  end

  test "should destroy task" do
    assert_difference('Task.count', -1) do
      delete :destroy, id: @task
    end

    assert_redirected_to tasks_path
  end
end
