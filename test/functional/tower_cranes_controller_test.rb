require 'test_helper'

class TowerCranesControllerTest < ActionController::TestCase
  setup do
    @tower_crane = tower_cranes(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:tower_cranes)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create tower_crane" do
    assert_difference('TowerCrane.count') do
      post :create, tower_crane: { baseWidth: @tower_crane.baseWidth, craneModel: @tower_crane.craneModel, draw_layer: @tower_crane.draw_layer, end_date: @tower_crane.end_date, end_time: @tower_crane.end_time, height: @tower_crane.height, name: @tower_crane.name, obj_class: @tower_crane.obj_class, raduis: @tower_crane.raduis, resource_pool: @tower_crane.resource_pool, rotation_speed: @tower_crane.rotation_speed, sim_obj_path: @tower_crane.sim_obj_path, start_date: @tower_crane.start_date, start_time: @tower_crane.start_time, trolly_speed: @tower_crane.trolly_speed, xPos: @tower_crane.xPos, yPos: @tower_crane.yPos, z_speed: @tower_crane.z_speed }
    end

    assert_redirected_to tower_crane_path(assigns(:tower_crane))
  end

  test "should show tower_crane" do
    get :show, id: @tower_crane
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @tower_crane
    assert_response :success
  end

  test "should update tower_crane" do
    put :update, id: @tower_crane, tower_crane: { baseWidth: @tower_crane.baseWidth, craneModel: @tower_crane.craneModel, draw_layer: @tower_crane.draw_layer, end_date: @tower_crane.end_date, end_time: @tower_crane.end_time, height: @tower_crane.height, name: @tower_crane.name, obj_class: @tower_crane.obj_class, raduis: @tower_crane.raduis, resource_pool: @tower_crane.resource_pool, rotation_speed: @tower_crane.rotation_speed, sim_obj_path: @tower_crane.sim_obj_path, start_date: @tower_crane.start_date, start_time: @tower_crane.start_time, trolly_speed: @tower_crane.trolly_speed, xPos: @tower_crane.xPos, yPos: @tower_crane.yPos, z_speed: @tower_crane.z_speed }
    assert_redirected_to tower_crane_path(assigns(:tower_crane))
  end

  test "should destroy tower_crane" do
    assert_difference('TowerCrane.count', -1) do
      delete :destroy, id: @tower_crane
    end

    assert_redirected_to tower_cranes_path
  end
end
